<?php

namespace Database\Factories;

use App\Models\Address;
use Faker\Provider\Address as FakerAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $lang = 49.2279;
        $long = -122.9605;

        return [
            'country_code' => 'US',
            'state' => $this->faker->state,
            'city' => $this->faker->city,
            'address' => $this->faker->address,
            'zipcode' => FakerAddress::postcode(),
            'latitude' => $this->faker->latitude(
                $min = ($lang * 10000 - rand(0, 50)) / 10000,
                $max = ($lang * 10000 + rand(0, 50)) / 10000
            ),
            'longitude' => $this->faker->longitude(
                $min = ($long * 10000 - rand(0, 50)) / 10000,
                $max = ($long * 10000 + rand(0, 50)) / 10000
            )
            //
        ];
    }
}
