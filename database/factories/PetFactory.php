<?php

namespace Database\Factories;

use App\Models\AnimalBreed;
use App\Models\Pet;
use Illuminate\Database\Eloquent\Factories\Factory;

class PetFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pet::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => $this->faker->uuid,
            'name' => $this->faker->firstName,
            'description' => $this->faker->sentence,
            'user_id' => $this->faker->numberBetween(1, 100),
            'age_type_id' => $this->faker->randomElement([1,3]),
            'coat_type_id' => $this->faker->numberBetween(0,6),
            'weight' => $this->faker->numberBetween(1, 200),
            'height' => $this->faker->numberBetween(1, 10),
            'measurement_type_id' => $this->faker->randomElement([0,1]),
            'gender' => $this->faker->numberBetween(1,2),
            'breed_id' => function () {
                $breed = AnimalBreed::all()->random(1)->first();
                return $breed->id;
            },
            'is_vaccinated' => $this->faker->boolean(),
            'microchip_number' => $this->faker->creditCardNumber,
            'reward_amount' => $this->faker->numberBetween(0, 500),
            'status' => $this->faker->randomElement([ 1, 2, 3]),
            'lost_date' => $this->faker->dateTimeBetween('-1 years'),
            'found_date' => $this->faker->dateTimeThisYear(),
            'contact_number' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            // 'posting_date' => $this->faker->dateTimeBetween('-7 days'),

            'coat_type_id' => $this->faker->numberBetween(1, 3),
            'instagram' => $this->faker->url,
            'facebook' => $this->faker->url
        ];
    }


  
}


