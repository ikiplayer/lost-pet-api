<?php

namespace Database\Factories;

use App\Models\Notification;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'user_id' => $this->faker->numberBetween(0,100),
            'object_id' => $this->faker->numberBetween(0, 100),
            'object_type' => $this->faker->numberBetween(0, 5),
            'message' => $this->faker->sentence,
            'has_viewed' => $this->faker->boolean()
        ];
    }
}
