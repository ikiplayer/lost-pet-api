<?php

namespace Database\Factories;

use App\Models\AnimalBreed;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnimalBreedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AnimalBreed::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
