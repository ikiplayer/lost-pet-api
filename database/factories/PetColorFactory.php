<?php

namespace Database\Factories;

use App\Models\PetColor;
use Illuminate\Database\Eloquent\Factories\Factory;

class PetColorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PetColor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'color' => $this->faker->randomElement([
                PetColor::COLOR_YELLOW_LABEL,
                PetColor::COLOR_BROWN_LABEL,
                PetColor::COLOR_WHITE_LABEL,
                PetColor::COLOR_BLACK_LABEL,
                PetColor::COLOR_GREY_LABEL,
                PetColor::COLOR_BLUE_LABEL,
            ])
            //
        ];
    }
}
