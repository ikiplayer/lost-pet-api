<?php

namespace Database\Factories;

use App\Models\PetMatchImage;
use Illuminate\Database\Eloquent\Factories\Factory;

class PetMatchImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PetMatchImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            // 'pet_id' => $this->faker->numberBetween(0,100),
            // 'image_id' => $this->faker->numberBetween(0,100),
            'match_percentage' => $this->faker->numberBetween(0, 100)

        ];
    }
}
