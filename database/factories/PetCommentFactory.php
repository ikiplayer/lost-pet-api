<?php

namespace Database\Factories;

use App\Models\PetComment;
use Illuminate\Database\Eloquent\Factories\Factory;

class PetCommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PetComment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'message' => $this->faker->sentence,
            'user_id' => $this->faker->numberBetween(0, 100)
        ];
    }
}
