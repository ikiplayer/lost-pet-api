<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->company,
            // 'hash_name' => $this->faker->md5,
            'url' => $this->faker->randomElement([
                'https://images.dog.ceo/breeds/papillon/n02086910_9355.jpg', 
                'https://images.dog.ceo/breeds/pomeranian/n02112018_6149.jpg',
                'https://images.dog.ceo/breeds/doberman/n02107142_5196.jpg',
                'https://images.dog.ceo/breeds/entlebucher/n02108000_2064.jpg',
                'https://images.dog.ceo/breeds/malinois/n02105162_7621.jpg',
                'https://images.dog.ceo/breeds/malinois/n02105162_5299.jpg',
                'https://images.dog.ceo/breeds/terrier-irish/n02093991_2663.jpg',
                'https://images.dog.ceo/breeds/redbone/n02090379_4683.jpg',
                'https://images.dog.ceo/breeds/doberman/n02107142_6816.jpg',
                'https://images.dog.ceo/breeds/spaniel-sussex/n02102480_4670.jpg'
                ]),
            'user_id' => $this->faker->numberBetween(0, 100),
        ];
    }
}
