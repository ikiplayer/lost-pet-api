<?php

namespace Database\Factories;

use App\Models\AnimalBreed;
use App\Models\ImageTag;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageTagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ImageTag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $animalTypeId = $this->faker->numberBetween(1,2);

        return [
            'user_age' => $this->faker->numberBetween(1, 20),
            'user_weight' => $this->faker->numberBetween(1, 200),
            'user_height' => $this->faker->numberBetween(1, 10),
            'user_gender' => $this->faker->numberBetween(1,2),
            // 'user_color' => $this->faker->colorName,
            'user_animal_type_id' => $animalTypeId,
            'user_breed_id' => function () use ($animalTypeId) {
                if ($animalTypeId == 1){
                    $breed = AnimalBreed::all()->where('animal_type_id', 1)->random(1)->first();
                } else {
                    $breed = AnimalBreed::all()->where('animal_type_id', 2)->random(1)->first();

                }
                return $breed->id;
            },
        ];
    }
}
