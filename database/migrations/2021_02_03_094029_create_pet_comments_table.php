<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_comments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pet_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('message')->nullable();
            $table->timestamps();
            $table->index(['pet_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_comments');
    }
}
