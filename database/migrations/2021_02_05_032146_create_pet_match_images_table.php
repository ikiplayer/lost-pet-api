<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetMatchImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_match_images', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pet_id')->nullable();
            $table->bigInteger('image_id')->nullable();
            $table->integer('match_percentage')->nullable();
            $table->timestamps();
            $table->index(['pet_id', 'image_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_match_images');
    }
}
