<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('src')->nullable();
            $table->string('url')->nullable();
            $table->string('file_name')->nullable();
            $table->bigInteger('imageable_id')->nullable();
            $table->string('imageable_type')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('user_uuid')->nullable();
            $table->boolean('is_main')->nullable()->default(0);
            $table->integer('order')->nullable();
            $table->timestamps();
            $table->index(['imageable_id', 'user_id', 'user_uuid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
