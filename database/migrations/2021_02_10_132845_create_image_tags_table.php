<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_tags', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('image_id')->nullable();
            $table->bigInteger('ai_animal_type_id')->nullable();
            $table->bigInteger('ai_breed_id')->nullable();
            $table->bigInteger('ai_age_type_id')->nullable();
            $table->bigInteger('ai_coat_type_id')->nullable();
            $table->timestamps();
            $table->index('image_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_tags');
    }
}
