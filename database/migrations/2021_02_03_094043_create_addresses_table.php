<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->string('country_code')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->integer('zipcode')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->bigInteger('lostable_id')->nullable();
            $table->string('lostable_type')->nullable();
            $table->bigInteger('foundable_id')->nullable();
            $table->string('foundable_type')->nullable();
            $table->bigInteger('unidentifiable_id')->nullable();
            $table->string('unidentifiable_type')->nullable();
            $table->bigInteger('addressable_id')->nullable();
            $table->string('addressable_type')->nullable();
            $table->timestamps();
            $table->index(['lostable_id', 'foundable_id', 'addressable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
