<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('name');
            $table->string('description')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('user_uuid')->nullable();
            $table->decimal('weight', 7, 2)->nullable();
            $table->decimal('height', 7, 2)->nullable();
            $table->integer('measurement_type_id')->nullable();
            $table->tinyInteger('gender')->default(0);
            $table->integer('breed_id')->nullable();
            $table->integer('age_type_id')->nullable();
            $table->string('coat_type_id')->nullable();
            $table->tinyInteger('is_vaccinated')->default(0);
            $table->string('microchip_number')->nullable();
            $table->integer('reward_amount')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamp('lost_date')->nullable();
            $table->timestamp('found_date')->nullable();
            $table->timestamp('unidentified_date')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('email')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('age_type_id');
            $table->index('coat_type_id');
            $table->index('measurement_type_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
