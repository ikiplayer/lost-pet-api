<?php

namespace Database\Seeders;

use App\Models\AnimalType;
use Illuminate\Database\Seeder;

class AnimalTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $newAnimalType = new AnimalType;
        $newAnimalType->id = 1;
        $newAnimalType->name = 'Dog';      
        $newAnimalType->save();  

        $newAnimalType = new AnimalType;
        $newAnimalType->id = 2;
        $newAnimalType->name = 'Cat';      
        $newAnimalType->save();  
    }
}
