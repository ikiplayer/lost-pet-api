<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\PetComment;
use App\Models\Image;
use App\Models\ImageTag;
use App\Models\Notification;
use App\Models\PetColor;
use App\Models\PetMatchImage;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AnimalTypeSeeder::class);
        $this->call(AnimalBreedSeeder::class);

        for ($i=0; $i < 500; $i++) { 
            $user = \App\Models\User::factory(1)->create()->first();
            Address::factory(1)->create(['addressable_id' => $user->id, 'addressable_type' => Address::CLASS_TYPE_USER])->first();
        }

        for ($i=0; $i < 500; $i++) { 
            $pet = \App\Models\Pet::factory(1)->create()->first();
            Address::factory(1)->create(['foundable_id' => $pet->id, 'foundable_type' => Address::CLASS_TYPE_FOUND_PET])->first();
            Address::factory(1)->create(['lostable_id' => $pet->id, 'lostable_type' => Address::CLASS_TYPE_LOST_PET])->first();
            $faker = Faker::create();
            $randomInt = $faker->numberBetween(0, 5);
            $matchedImage = Image::factory(1)->create(['imageable_id' => $pet->id, 'imageable_type' => Image::CLASS_TYPE_PET])->first();
            PetMatchImage::factory(1)->create(['pet_id' => $faker->numberBetween(1,100), 'image_id' => $faker->numberBetween(1,100)]);
            $imageTag = ImageTag::factory(1)->create(['image_id' => $faker->numberBetween(1,100)])->first();
            for ($j=0; $j < $randomInt; $j++) { 
                PetComment::factory(1)->create(['pet_id' => $pet->id]);
                Image::factory(1)->create(['imageable_id' => $pet->id, 'imageable_type' => Image::CLASS_TYPE_PET]);
                PetColor::factory(1)->create(['colorable_id' => $pet->id, 'colorable_type' => PetColor::CLASS_TYPE_PET]);
                PetColor::factory(1)->create(['colorable_id' => $imageTag->id, 'colorable_type' => PetColor::CLASS_TYPE_IMAGE_TAG]);
            }
           
        }

        Notification::factory(500)->create()->first();
    }
}
