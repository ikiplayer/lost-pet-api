<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin: *');// // //Access-Control-Allow-Origin: *
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  X-Requested-With, Content-Type, X-Auth-Token, Origin, Authorization');


Route::post('auth/register', '\App\Http\Controllers\Client\AuthController@register');
Route::post('auth/login', '\App\Http\Controllers\Client\AuthController@login');
Route::post('auth/forgot', '\App\Http\Controllers\Client\PasswordResetController@create');
Route::post('auth/validate/registration', '\App\Http\Controllers\Client\AuthController@verifyRegistration');
Route::post('auth/validate/password-reset', '\App\Http\Controllers\Client\PasswordResetController@find');
Route::post('auth/password-reset', '\App\Http\Controllers\Client\PasswordResetController@reset');
Route::post('auth/password-forgot', '\App\Http\Controllers\Client\PasswordResetController@createForgot');

Route::group([
    'prefix' => 'admin',
    'middleware' => [
    // 'auth:api', 
    // 'role'
    ],
], function () {
    // Route::get('user/{id}', '\App\Http\Controllers\Admin\UserController@show');
    // Route::get('user', '\App\Http\Controllers\Admin\UserController@index');
    // Route::post('user', '\App\Http\Controllers\Admin\UserController@store');
    // Route::put('user/{id}', '\App\Http\Controllers\Admin\UserController@update');
    // Route::delete('user/{id}', '\App\Http\Controllers\Admin\UserController@destroy');

});

Route::group([
    'prefix' => 'user',
    'middleware' => [
    'auth:api', 
    // 'role'
    ],
], function () {
    Route::get('me', '\App\Http\Controllers\Client\UserController@show');
    Route::post('me', '\App\Http\Controllers\Client\UserController@update');
    Route::put('pet/found/{uuid}', '\App\Http\Controllers\Client\PetController@updatePetFound');
    Route::apiResource('pet', '\App\Http\Controllers\Client\PetController');
    Route::get('pet-count', '\App\Http\Controllers\Client\PetController@petCount');
    Route::get('notification', '\App\Http\Controllers\Client\PetController@getMyNotifications');
    Route::post('pet/image/upload', '\App\Http\Controllers\Client\PetController@addPetImages');
    Route::delete('pet/image/{id}', '\App\Http\Controllers\Client\PetController@deleteImageByUser');
    Route::post('password-reset', '\App\Http\Controllers\Client\PasswordResetController@createReset');
    // Payment
    Route::get('payment/setup-intent', '\App\Http\Controllers\Client\PaymentController@getSetupIntent');
    Route::post('payment', '\App\Http\Controllers\Client\PaymentController@paymentProcess');

    
});


// ANYONE
Route::get('pet', '\App\Http\Controllers\PetController@index');
Route::get('pet/{uuid}', '\App\Http\Controllers\PetController@show');

// FIX THE PET POSTER ISSUE
// Route::get('pet-poster/{uuid}/{type}', '\App\Http\Controllers\PetController@generatePetPoster');
Route::post('lost-pet/upload', '\App\Http\Controllers\PetController@uploadLostPet');
// Route::get('test-process-image', '\App\Http\Controllers\PetController@testProcessImage');

// LOOKUP
Route::get('lookup/pet-count', '\App\Http\Controllers\LookupController@petCount');
Route::get('lookup/coat-type', '\App\Http\Controllers\LookupController@petCoatType');
Route::get('lookup/color', '\App\Http\Controllers\LookupController@petColors');
Route::get('lookup/breed', '\App\Http\Controllers\LookupController@petBreeds');
Route::get('lookup/animal', '\App\Http\Controllers\LookupController@animalTypes');


