# Endpoints


## api/auth/register




> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"first_name":"John","last_name":"Doe","email":"johndoe@test.com","password":"Test123","confirm_password":"Test123"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "first_name": "John",
    "last_name": "Doe",
    "email": "johndoe@test.com",
    "password": "Test123",
    "confirm_password": "Test123"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-auth-register" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-register"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-register"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-register"></code></pre>
</div>
<form id="form-POSTapi-auth-register" data-method="POST" data-path="api/auth/register" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-register', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-register" onclick="tryItOut('POSTapi-auth-register');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-register" onclick="cancelTryOut('POSTapi-auth-register');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-register" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/register</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>first_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="first_name" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
The first name of user.</p>
<p>
<b><code>last_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="last_name" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
The last name of user.</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
The email of the user.</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>confirm_password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="confirm_password" data-endpoint="POSTapi-auth-register" data-component="body" required  hidden>
<br>
</p>

</form>


## api/auth/login




> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"johndoe@test.com","password":"Test123"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "johndoe@test.com",
    "password": "Test123"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-auth-login" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-login"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-login"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-login"></code></pre>
</div>
<form id="form-POSTapi-auth-login" data-method="POST" data-path="api/auth/login" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-login', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-login" onclick="tryItOut('POSTapi-auth-login');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-login" onclick="cancelTryOut('POSTapi-auth-login');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-login" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/login</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-login" data-component="body" required  hidden>
<br>
The email of the user.</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password" data-endpoint="POSTapi-auth-login" data-component="body" required  hidden>
<br>
</p>

</form>


## api/auth/forgot




> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/forgot" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"johndoe@test.com"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/forgot"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "johndoe@test.com"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-auth-forgot" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-forgot"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-forgot"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-forgot" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-forgot"></code></pre>
</div>
<form id="form-POSTapi-auth-forgot" data-method="POST" data-path="api/auth/forgot" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-forgot', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-forgot" onclick="tryItOut('POSTapi-auth-forgot');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-forgot" onclick="cancelTryOut('POSTapi-auth-forgot');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-forgot" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/forgot</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-forgot" data-component="body" required  hidden>
<br>
</p>

</form>


## api/auth/password-reset




> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/password-reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"fugit","password":"rerum","password_confirmation":"animi","token":"porro"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/password-reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "fugit",
    "password": "rerum",
    "password_confirmation": "animi",
    "token": "porro"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-auth-password-reset" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-auth-password-reset"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-auth-password-reset"></code></pre>
</div>
<div id="execution-error-POSTapi-auth-password-reset" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-auth-password-reset"></code></pre>
</div>
<form id="form-POSTapi-auth-password-reset" data-method="POST" data-path="api/auth/password-reset" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-auth-password-reset', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-auth-password-reset" onclick="tryItOut('POSTapi-auth-password-reset');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-auth-password-reset" onclick="cancelTryOut('POSTapi-auth-password-reset');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-auth-password-reset" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/auth/password-reset</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-auth-password-reset" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password" data-endpoint="POSTapi-auth-password-reset" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>password_confirmation</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="password_confirmation" data-endpoint="POSTapi-auth-password-reset" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>token</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="token" data-endpoint="POSTapi-auth-password-reset" data-component="body" required  hidden>
<br>
</p>

</form>


## api/user/pet




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/pet?sort_by=created_at&status=2&descending=DESC&search=golden&gender=2&zipcode=46228" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/pet"
);

let params = {
    "sort_by": "created_at",
    "status": "2",
    "descending": "DESC",
    "search": "golden",
    "gender": "2",
    "zipcode": "46228",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [],
    "links": {
        "first": "http:\/\/localhost\/api\/user\/pet?page=1",
        "last": "http:\/\/localhost\/api\/user\/pet?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": null,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/user\/pet?page=1",
                "label": 1,
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/localhost\/api\/user\/pet",
        "per_page": 15,
        "to": null,
        "total": 0
    }
}
```
<div id="execution-results-GETapi-user-pet" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user-pet"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user-pet"></code></pre>
</div>
<div id="execution-error-GETapi-user-pet" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user-pet"></code></pre>
</div>
<form id="form-GETapi-user-pet" data-method="GET" data-path="api/user/pet" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user-pet', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user-pet" onclick="tryItOut('GETapi-user-pet');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user-pet" onclick="cancelTryOut('GETapi-user-pet');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user-pet" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user/pet</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>sort_by</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="sort_by" data-endpoint="GETapi-user-pet" data-component="query"  hidden>
<br>
Sort by [key].</p>
<p>
<b><code>status</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="status" data-endpoint="GETapi-user-pet" data-component="query"  hidden>
<br>
LOST OR FOUND 1|2.</p>
<p>
<b><code>descending</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="descending" data-endpoint="GETapi-user-pet" data-component="query"  hidden>
<br>
DESC|ASC.</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-user-pet" data-component="query"  hidden>
<br>
</p>
<p>
<b><code>gender</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="gender" data-endpoint="GETapi-user-pet" data-component="query"  hidden>
<br>
1|2.</p>
<p>
<b><code>zipcode</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="zipcode" data-endpoint="GETapi-user-pet" data-component="query"  hidden>
<br>
</p>
</form>


## api/user/pet




> Example request:

```bash
curl -X POST \
    "http://localhost/api/user/pet" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"John Rocky","description":"I lost it on friday","age_type":{"id":1},"color_type":[],"color_type_id":1,"weight":10,"height":63,"measurement_type":{"id":1},"gender":1,"breed_id":1,"is_vaccinated":1,"has_microchip":1,"reward_amount":100,"status":{"id":1},"lost_date":"2021-01-11 03:17:54","found_date":"2021-02-15 03:17:54","contact_number":"012345678","instagram":0,"email":"test@test.com","lost_location":{"country_code":"US","state":"Indiana","city":"Fishers","zipcode":"46228","address":"123 Main Street","longitude":"39.9568","latitude":"86.0134","lostable_id":"odit"},"found_location":{"country_code":"US","state":"Illinois","city":"Chicago","address":"321 West Street","longitude":"41.8781","latitude":"87.6298","foundable_id":"omnis"}}'

```

```javascript
const url = new URL(
    "http://localhost/api/user/pet"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "John Rocky",
    "description": "I lost it on friday",
    "age_type": {
        "id": 1
    },
    "color_type": [],
    "color_type_id": 1,
    "weight": 10,
    "height": 63,
    "measurement_type": {
        "id": 1
    },
    "gender": 1,
    "breed_id": 1,
    "is_vaccinated": 1,
    "has_microchip": 1,
    "reward_amount": 100,
    "status": {
        "id": 1
    },
    "lost_date": "2021-01-11 03:17:54",
    "found_date": "2021-02-15 03:17:54",
    "contact_number": "012345678",
    "instagram": 0,
    "email": "test@test.com",
    "lost_location": {
        "country_code": "US",
        "state": "Indiana",
        "city": "Fishers",
        "zipcode": "46228",
        "address": "123 Main Street",
        "longitude": "39.9568",
        "latitude": "86.0134",
        "lostable_id": "odit"
    },
    "found_location": {
        "country_code": "US",
        "state": "Illinois",
        "city": "Chicago",
        "address": "321 West Street",
        "longitude": "41.8781",
        "latitude": "87.6298",
        "foundable_id": "omnis"
    }
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-POSTapi-user-pet" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-user-pet"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-user-pet"></code></pre>
</div>
<div id="execution-error-POSTapi-user-pet" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-user-pet"></code></pre>
</div>
<form id="form-POSTapi-user-pet" data-method="POST" data-path="api/user/pet" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-user-pet', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-user-pet" onclick="tryItOut('POSTapi-user-pet');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-user-pet" onclick="cancelTryOut('POSTapi-user-pet');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-user-pet" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/user/pet</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="description" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>age_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>age_type.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="age_type.id" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>color_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<input type="text" name="color_type" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>color_type_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="color_type_id" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>weight</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="weight" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>height</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="height" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>measurement_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>measurement_type.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="measurement_type.id" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>gender</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="gender" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>breed_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="breed_id" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>is_vaccinated</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="is_vaccinated" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>has_microchip</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="has_microchip" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>reward_amount</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="reward_amount" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>status</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>status.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="status.id" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>lost_date</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_date" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_date</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_date" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>contact_number</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="contact_number" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>instagram</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="instagram" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>lost_location</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>lost_location.country_code</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.country_code" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.state</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.state" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.city</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.city" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.zipcode</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.zipcode" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.address" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.longitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.longitude" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.latitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.latitude" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.lostable_id</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.lostable_id" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<details>
<summary>
<b><code>found_location</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>found_location.country_code</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.country_code" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.state</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.state" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.city</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.city" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.address" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.longitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.longitude" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.latitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.latitude" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.foundable_id</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.foundable_id" data-endpoint="POSTapi-user-pet" data-component="body"  hidden>
<br>
</p>
</details>
</p>

</form>


## api/user/pet/{pet}




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/pet/fugiat" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/pet/fugiat"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500):

```json
{
    "message": "Trying to get property 'id' of non-object",
    "exception": "ErrorException",
    "file": "\/var\/www\/html\/app\/Http\/Controllers\/Client\/PetController.php",
    "line": 156,
    "trace": [
        {
            "file": "\/var\/www\/html\/app\/Http\/Controllers\/Client\/PetController.php",
            "line": 156,
            "function": "handleError",
            "class": "Illuminate\\Foundation\\Bootstrap\\HandleExceptions",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php",
            "line": 54,
            "function": "show",
            "class": "App\\Http\\Controllers\\Client\\PetController",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 254,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 197,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 693,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 50,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 127,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 103,
            "function": "handleRequest",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 55,
            "function": "handleRequestUsingNamedLimiter",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 610,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 93,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-GETapi-user-pet--pet-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user-pet--pet-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user-pet--pet-"></code></pre>
</div>
<div id="execution-error-GETapi-user-pet--pet-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user-pet--pet-"></code></pre>
</div>
<form id="form-GETapi-user-pet--pet-" data-method="GET" data-path="api/user/pet/{pet}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user-pet--pet-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user-pet--pet-" onclick="tryItOut('GETapi-user-pet--pet-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user-pet--pet-" onclick="cancelTryOut('GETapi-user-pet--pet-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user-pet--pet-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user/pet/{pet}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>pet</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="pet" data-endpoint="GETapi-user-pet--pet-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>uuid</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="uuid" data-endpoint="GETapi-user-pet--pet-" data-component="url"  hidden>
<br>
</p>
</form>


## api/user/pet/{pet}




> Example request:

```bash
curl -X PUT \
    "http://localhost/api/user/pet/ab" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"John Rocky","description":"I lost it on friday","age_type":{"id":1},"color_type":{"id":1},"weight":10,"height":63,"measurement_type":{"id":1},"gender":1,"breed":{"id":1},"is_vaccinated":1,"has_microchip":1,"microchip_number":12345678,"reward_amount":100,"status":{"id":1},"lost_date":"2021-01-11 03:17:54","found_date":"2021-02-15 03:17:54","contact_number":"012345678","instagram":0,"email":"test@test.com","lost_location":{"country_code":"US","state":"Indiana","city":"Fishers","address":"123 Main Street","zipcode":"46228","longitude":"39.9568","latitude":"86.0134","lostable_id":"neque"},"found_location":{"country_code":"US","state":"Chicago","city":"Illinois","address":"321 West Street","zipcode":"46228","longitude":"41.8781","latitude":"87.6298","foundable_id":"veritatis"}}'

```

```javascript
const url = new URL(
    "http://localhost/api/user/pet/ab"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "John Rocky",
    "description": "I lost it on friday",
    "age_type": {
        "id": 1
    },
    "color_type": {
        "id": 1
    },
    "weight": 10,
    "height": 63,
    "measurement_type": {
        "id": 1
    },
    "gender": 1,
    "breed": {
        "id": 1
    },
    "is_vaccinated": 1,
    "has_microchip": 1,
    "microchip_number": 12345678,
    "reward_amount": 100,
    "status": {
        "id": 1
    },
    "lost_date": "2021-01-11 03:17:54",
    "found_date": "2021-02-15 03:17:54",
    "contact_number": "012345678",
    "instagram": 0,
    "email": "test@test.com",
    "lost_location": {
        "country_code": "US",
        "state": "Indiana",
        "city": "Fishers",
        "address": "123 Main Street",
        "zipcode": "46228",
        "longitude": "39.9568",
        "latitude": "86.0134",
        "lostable_id": "neque"
    },
    "found_location": {
        "country_code": "US",
        "state": "Chicago",
        "city": "Illinois",
        "address": "321 West Street",
        "zipcode": "46228",
        "longitude": "41.8781",
        "latitude": "87.6298",
        "foundable_id": "veritatis"
    }
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


<div id="execution-results-PUTapi-user-pet--pet-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-user-pet--pet-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-user-pet--pet-"></code></pre>
</div>
<div id="execution-error-PUTapi-user-pet--pet-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-user-pet--pet-"></code></pre>
</div>
<form id="form-PUTapi-user-pet--pet-" data-method="PUT" data-path="api/user/pet/{pet}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-user-pet--pet-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-user-pet--pet-" onclick="tryItOut('PUTapi-user-pet--pet-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-user-pet--pet-" onclick="cancelTryOut('PUTapi-user-pet--pet-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-user-pet--pet-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/user/pet/{pet}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/user/pet/{pet}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>pet</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="pet" data-endpoint="PUTapi-user-pet--pet-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>uuid</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="uuid" data-endpoint="PUTapi-user-pet--pet-" data-component="url"  hidden>
<br>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="name" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="description" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>age_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>age_type.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="age_type.id" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<details>
<summary>
<b><code>color_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>color_type.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="color_type.id" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>weight</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="weight" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>height</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="height" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>measurement_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>measurement_type.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="measurement_type.id" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>gender</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="gender" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>breed</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>breed.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="breed.id" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>is_vaccinated</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="is_vaccinated" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>has_microchip</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="has_microchip" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>microchip_number</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="microchip_number" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>reward_amount</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="reward_amount" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>status</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>status.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="status.id" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>lost_date</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_date" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_date</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_date" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>contact_number</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="contact_number" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>instagram</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="instagram" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>lost_location</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>lost_location.country_code</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.country_code" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.state</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.state" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.city</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.city" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.address" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.zipcode</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.zipcode" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.longitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.longitude" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.latitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.latitude" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>lost_location.lostable_id</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="lost_location.lostable_id" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
</details>
</p>
<p>
<details>
<summary>
<b><code>found_location</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>found_location.country_code</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.country_code" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.state</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.state" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.city</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.city" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.address" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.zipcode</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.zipcode" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.longitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.longitude" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.latitude</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.latitude" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
<p>
<b><code>found_location.foundable_id</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="found_location.foundable_id" data-endpoint="PUTapi-user-pet--pet-" data-component="body"  hidden>
<br>
</p>
</details>
</p>

</form>


## api/user/pet/{pet}




> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/user/pet/rerum" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/pet/rerum"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEapi-user-pet--pet-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-user-pet--pet-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-user-pet--pet-"></code></pre>
</div>
<div id="execution-error-DELETEapi-user-pet--pet-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-user-pet--pet-"></code></pre>
</div>
<form id="form-DELETEapi-user-pet--pet-" data-method="DELETE" data-path="api/user/pet/{pet}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-user-pet--pet-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-user-pet--pet-" onclick="tryItOut('DELETEapi-user-pet--pet-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-user-pet--pet-" onclick="cancelTryOut('DELETEapi-user-pet--pet-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-user-pet--pet-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/user/pet/{pet}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>pet</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="pet" data-endpoint="DELETEapi-user-pet--pet-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>uuid</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="uuid" data-endpoint="DELETEapi-user-pet--pet-" data-component="url"  hidden>
<br>
</p>
</form>


## api/user/notification




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user/notification" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/notification"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (500):

```json
{
    "message": "Trying to get property 'id' of non-object",
    "exception": "ErrorException",
    "file": "\/var\/www\/html\/app\/Http\/Controllers\/Client\/PetController.php",
    "line": 330,
    "trace": [
        {
            "file": "\/var\/www\/html\/app\/Http\/Controllers\/Client\/PetController.php",
            "line": 330,
            "function": "handleError",
            "class": "Illuminate\\Foundation\\Bootstrap\\HandleExceptions",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Controller.php",
            "line": 54,
            "function": "getMyNotifications",
            "class": "App\\Http\\Controllers\\Client\\PetController",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 254,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Route.php",
            "line": 197,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 693,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/SubstituteBindings.php",
            "line": 50,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 127,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 103,
            "function": "handleRequest",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Middleware\/ThrottleRequests.php",
            "line": 55,
            "function": "handleRequestUsingNamedLimiter",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Routing\/Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Middleware\/PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/fruitcake\/laravel-cors\/src\/HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/fideloper\/proxy\/src\/TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Pipeline\/Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Http\/Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Strategies\/Responses\/ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Extracting\/Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/knuckleswtf\/scribe\/src\/Commands\/GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Container\/Container.php",
            "line": 610,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Command\/Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/symfony\/console\/Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Console\/Application.php",
            "line": 93,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/vendor\/laravel\/framework\/src\/Illuminate\/Foundation\/Console\/Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "\/var\/www\/html\/artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-GETapi-user-notification" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user-notification"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user-notification"></code></pre>
</div>
<div id="execution-error-GETapi-user-notification" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user-notification"></code></pre>
</div>
<form id="form-GETapi-user-notification" data-method="GET" data-path="api/user/notification" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user-notification', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user-notification" onclick="tryItOut('GETapi-user-notification');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user-notification" onclick="cancelTryOut('GETapi-user-notification');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user-notification" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user/notification</code></b>
</p>
</form>


## api/user/pet/{petId}/image/upload




> Example request:

```bash
curl -X POST \
    "http://localhost/api/user/pet/aut/image/upload" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -F "images[]=@/tmp/phpRz5aG5" 
```

```javascript
const url = new URL(
    "http://localhost/api/user/pet/aut/image/upload"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
};

const body = new FormData();
body.append('images[]', document.querySelector('input[name="images[]"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-user-pet--petId--image-upload" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-user-pet--petId--image-upload"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-user-pet--petId--image-upload"></code></pre>
</div>
<div id="execution-error-POSTapi-user-pet--petId--image-upload" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-user-pet--petId--image-upload"></code></pre>
</div>
<form id="form-POSTapi-user-pet--petId--image-upload" data-method="POST" data-path="api/user/pet/{petId}/image/upload" data-authed="0" data-hasfiles="1" data-headers='{"Content-Type":"multipart\/form-data","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-user-pet--petId--image-upload', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-user-pet--petId--image-upload" onclick="tryItOut('POSTapi-user-pet--petId--image-upload');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-user-pet--petId--image-upload" onclick="cancelTryOut('POSTapi-user-pet--petId--image-upload');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-user-pet--petId--image-upload" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/user/pet/{petId}/image/upload</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>petId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="petId" data-endpoint="POSTapi-user-pet--petId--image-upload" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>uuid</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="uuid" data-endpoint="POSTapi-user-pet--petId--image-upload" data-component="url"  hidden>
<br>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>images</code></b>&nbsp;&nbsp;<small>file[]</small>     <i>optional</i> &nbsp;
<input type="file" name="images.0" data-endpoint="POSTapi-user-pet--petId--image-upload" data-component="body"  hidden>
<input type="file" name="images.1" data-endpoint="POSTapi-user-pet--petId--image-upload" data-component="body" hidden>
<br>
</p>

</form>


## api/user/pet/{petId}/photo/{photoId}




> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/user/pet/tempora/photo/aut" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user/pet/tempora/photo/aut"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


<div id="execution-results-DELETEapi-user-pet--petId--photo--photoId-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-user-pet--petId--photo--photoId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-user-pet--petId--photo--photoId-"></code></pre>
</div>
<div id="execution-error-DELETEapi-user-pet--petId--photo--photoId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-user-pet--petId--photo--photoId-"></code></pre>
</div>
<form id="form-DELETEapi-user-pet--petId--photo--photoId-" data-method="DELETE" data-path="api/user/pet/{petId}/photo/{photoId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-user-pet--petId--photo--photoId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-user-pet--petId--photo--photoId-" onclick="tryItOut('DELETEapi-user-pet--petId--photo--photoId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-user-pet--petId--photo--photoId-" onclick="cancelTryOut('DELETEapi-user-pet--petId--photo--photoId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-user-pet--petId--photo--photoId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/user/pet/{petId}/photo/{photoId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>petId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="petId" data-endpoint="DELETEapi-user-pet--petId--photo--photoId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>photoId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="photoId" data-endpoint="DELETEapi-user-pet--petId--photo--photoId-" data-component="url" required  hidden>
<br>
</p>
<p>
<b><code>uuid</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="uuid" data-endpoint="DELETEapi-user-pet--petId--photo--photoId-" data-component="url"  hidden>
<br>
</p>
<p>
<b><code>hashname</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="hashname" data-endpoint="DELETEapi-user-pet--petId--photo--photoId-" data-component="url"  hidden>
<br>
</p>
</form>


## api/pet




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/pet?sort_by=created_at&status=2&breed_type[id]=1&descending=DESC&gender=2&search=golden&zipcode=46228" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/pet"
);

let params = {
    "sort_by": "created_at",
    "status": "2",
    "breed_type[id]": "1",
    "descending": "DESC",
    "gender": "2",
    "search": "golden",
    "zipcode": "46228",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [],
    "links": {
        "first": "http:\/\/localhost\/api\/pet?page=1",
        "last": "http:\/\/localhost\/api\/pet?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": null,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http:\/\/localhost\/api\/pet?page=1",
                "label": 1,
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/localhost\/api\/pet",
        "per_page": 15,
        "to": null,
        "total": 0
    }
}
```
<div id="execution-results-GETapi-pet" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-pet"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-pet"></code></pre>
</div>
<div id="execution-error-GETapi-pet" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-pet"></code></pre>
</div>
<form id="form-GETapi-pet" data-method="GET" data-path="api/pet" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-pet', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-pet" onclick="tryItOut('GETapi-pet');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-pet" onclick="cancelTryOut('GETapi-pet');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-pet" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/pet</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
<p>
<b><code>sort_by</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="sort_by" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
Sort by [key].</p>
<p>
<b><code>status</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="status" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
LOST OR FOUND 1|2.</p>
<p>
<b><code>breed_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<input type="text" name="breed_type" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
</p>
<p>
<b><code>breed_type.id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="breed_type.id" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
</p>
<p>
<b><code>descending</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="descending" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
DESC|ASC.</p>
<p>
<b><code>gender</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="gender" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
1|2.</p>
<p>
<b><code>search</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="search" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
</p>
<p>
<b><code>zipcode</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="zipcode" data-endpoint="GETapi-pet" data-component="query"  hidden>
<br>
</p>
</form>


## api/lost-pet/upload




> Example request:

```bash
curl -X POST \
    "http://localhost/api/lost-pet/upload" \
    -H "Content-Type: multipart/form-data" \
    -H "Accept: application/json" \
    -F "breed[id]=10" \
    -F "age_type[id]=1" \
    -F "color_type[id]=0" \
    -F "gender=1" \
    -F "images[]=@/tmp/php04t6C5" 
```

```javascript
const url = new URL(
    "http://localhost/api/lost-pet/upload"
);

let headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
};

const body = new FormData();
body.append('breed[id]', '10');
body.append('age_type[id]', '1');
body.append('color_type[id]', '0');
body.append('gender', '1');
body.append('images[]', document.querySelector('input[name="images[]"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response => response.json());
```


<div id="execution-results-POSTapi-lost-pet-upload" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-lost-pet-upload"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-lost-pet-upload"></code></pre>
</div>
<div id="execution-error-POSTapi-lost-pet-upload" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-lost-pet-upload"></code></pre>
</div>
<form id="form-POSTapi-lost-pet-upload" data-method="POST" data-path="api/lost-pet/upload" data-authed="0" data-hasfiles="1" data-headers='{"Content-Type":"multipart\/form-data","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-lost-pet-upload', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-lost-pet-upload" onclick="tryItOut('POSTapi-lost-pet-upload');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-lost-pet-upload" onclick="cancelTryOut('POSTapi-lost-pet-upload');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-lost-pet-upload" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/lost-pet/upload</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>images</code></b>&nbsp;&nbsp;<small>file[]</small>  &nbsp;
<input type="file" name="images.0" data-endpoint="POSTapi-lost-pet-upload" data-component="body" required  hidden>
<input type="file" name="images.1" data-endpoint="POSTapi-lost-pet-upload" data-component="body" hidden>
<br>
</p>
<p>
<details>
<summary>
<b><code>animal_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>animal_type.id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="animal_type.id" data-endpoint="POSTapi-lost-pet-upload" data-component="body" required  hidden>
<br>
</p>
</details>
</p>
<p>
<details>
<summary>
<b><code>breed</code></b>&nbsp;&nbsp;<small>object</small>  &nbsp;
<br>
</summary>
<br>
<p>
<b><code>breed.id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="breed.id" data-endpoint="POSTapi-lost-pet-upload" data-component="body" required  hidden>
<br>
</p>
</details>
</p>
<p>
<details>
<summary>
<b><code>age_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>age_type.id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="age_type.id" data-endpoint="POSTapi-lost-pet-upload" data-component="body" required  hidden>
<br>
</p>
</details>
</p>
<p>
<details>
<summary>
<b><code>color_type</code></b>&nbsp;&nbsp;<small>object</small>     <i>optional</i> &nbsp;
<br>
</summary>
<br>
<p>
<b><code>color_type.id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="color_type.id" data-endpoint="POSTapi-lost-pet-upload" data-component="body" required  hidden>
<br>
</p>
</details>
</p>
<p>
<b><code>gender</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="gender" data-endpoint="POSTapi-lost-pet-upload" data-component="body" required  hidden>
<br>
</p>

</form>


## api/lookup/pet-count




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/lookup/pet-count" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/lookup/pet-count"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "message": "",
    "data": {
        "lost_pet_count": true,
        "found_pet_count": true
    }
}
```
<div id="execution-results-GETapi-lookup-pet-count" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-lookup-pet-count"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-lookup-pet-count"></code></pre>
</div>
<div id="execution-error-GETapi-lookup-pet-count" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-lookup-pet-count"></code></pre>
</div>
<form id="form-GETapi-lookup-pet-count" data-method="GET" data-path="api/lookup/pet-count" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-lookup-pet-count', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-lookup-pet-count" onclick="tryItOut('GETapi-lookup-pet-count');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-lookup-pet-count" onclick="cancelTryOut('GETapi-lookup-pet-count');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-lookup-pet-count" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/lookup/pet-count</code></b>
</p>
</form>


## api/lookup/coat-type




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/lookup/coat-type" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/lookup/coat-type"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "message": "Retrieving all coat types",
    "data": [
        {
            "id": "COAT_TYPE_HAIRLESS",
            "name": "COAT_TYPE_HAIRLESS_LABEL"
        },
        {
            "id": "COAT_TYPE_SHORT",
            "name": "COAT_TYPE_SHORT_LABEL"
        },
        {
            "id": "COAT_TYPE_WIRY",
            "name": "COAT_TYPE_WIRY_LABEL"
        },
        {
            "id": "COAT_TYPE_LONG",
            "name": "COAT_TYPE_LONG_LABEL"
        },
        {
            "id": "COAT_TYPE_CURLY",
            "name": "COAT_TYPE_CURLY_LABEL"
        },
        {
            "id": "COAT_TYPE_DOUBLE_COATED",
            "name": "COAT_TYPE_DOUBLE_COATED_LABEL"
        }
    ]
}
```
<div id="execution-results-GETapi-lookup-coat-type" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-lookup-coat-type"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-lookup-coat-type"></code></pre>
</div>
<div id="execution-error-GETapi-lookup-coat-type" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-lookup-coat-type"></code></pre>
</div>
<form id="form-GETapi-lookup-coat-type" data-method="GET" data-path="api/lookup/coat-type" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-lookup-coat-type', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-lookup-coat-type" onclick="tryItOut('GETapi-lookup-coat-type');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-lookup-coat-type" onclick="cancelTryOut('GETapi-lookup-coat-type');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-lookup-coat-type" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/lookup/coat-type</code></b>
</p>
</form>


## api/lookup/color




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/lookup/color" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/lookup/color"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "message": "Retrieving all pet colors",
    "data": [
        "Yellow",
        "Brown",
        "White",
        "Black",
        "Grey",
        "Blue"
    ]
}
```
<div id="execution-results-GETapi-lookup-color" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-lookup-color"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-lookup-color"></code></pre>
</div>
<div id="execution-error-GETapi-lookup-color" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-lookup-color"></code></pre>
</div>
<form id="form-GETapi-lookup-color" data-method="GET" data-path="api/lookup/color" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-lookup-color', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-lookup-color" onclick="tryItOut('GETapi-lookup-color');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-lookup-color" onclick="cancelTryOut('GETapi-lookup-color');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-lookup-color" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/lookup/color</code></b>
</p>
</form>


## api/lookup/breed/{typeId}




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/lookup/breed/eos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/lookup/breed/eos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": []
}
```
<div id="execution-results-GETapi-lookup-breed--typeId-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-lookup-breed--typeId-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-lookup-breed--typeId-"></code></pre>
</div>
<div id="execution-error-GETapi-lookup-breed--typeId-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-lookup-breed--typeId-"></code></pre>
</div>
<form id="form-GETapi-lookup-breed--typeId-" data-method="GET" data-path="api/lookup/breed/{typeId}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-lookup-breed--typeId-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-lookup-breed--typeId-" onclick="tryItOut('GETapi-lookup-breed--typeId-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-lookup-breed--typeId-" onclick="cancelTryOut('GETapi-lookup-breed--typeId-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-lookup-breed--typeId-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/lookup/breed/{typeId}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>typeId</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="typeId" data-endpoint="GETapi-lookup-breed--typeId-" data-component="url" required  hidden>
<br>
</p>
</form>


## api/lookup/animal




> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/lookup/animal" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/lookup/animal"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json

[]
```
<div id="execution-results-GETapi-lookup-animal" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-lookup-animal"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-lookup-animal"></code></pre>
</div>
<div id="execution-error-GETapi-lookup-animal" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-lookup-animal"></code></pre>
</div>
<form id="form-GETapi-lookup-animal" data-method="GET" data-path="api/lookup/animal" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-lookup-animal', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-lookup-animal" onclick="tryItOut('GETapi-lookup-animal');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-lookup-animal" onclick="cancelTryOut('GETapi-lookup-animal');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-lookup-animal" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/lookup/animal</code></b>
</p>
</form>



