<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
    <body>
        <div>
            <div class="text-center" style="font-size: 10vw">LOST {{Str::upper($pet->breed->animalType->name) }}</div>
            <div class="">
                <img class="object-contain h-auto w-9/12 m-auto border-8 border-black" src="{{$pet->images[0]->url}}"/>
            </div>
            <div class="text-center" style="font-size: 4vw">{{Str::upper($pet->name) }}</div>
            {{-- <div class="text-center" style="font-size: 1.5vw; width: 40vw; margin: auto">
                Lost in {{ optional($pet->lostLocation)->address .','
                .optional($pet->lostLocation)->city. ','
                .optional($pet->lostLocation)->state}}
            </div> --}}
            <div class="bg-indigo-600 mt-10"> 
                <div class="text-white text-center text-3xl pt-5">Please Contact at</div>
                <div class="grid grid-cols-2 gap-8">
                    <div class="text-white text-center text-3xl p-4">{{ $pet->user->email }}</div>
                    <div class="text-white text-center text-3xl p-4">{{ $pet->user->phone_number }}</div>
                </div>
            </div>
            <div>
                <div class="grid grid-cols-6">
                    @for ($i = 0; $i < 6; $i++)
                    <div class="border-dashed" style="border-width: 1px">
                        <div class="text-md text-center m-auto my-4" style="writing-mode: vertical-rl; text-orientation: mixed;">{{ $pet->user->email }}<br>{{ $pet->user->phone_number }}</div>
                    </div>
                    @endfor
                </div>

            </div>
        </div>
    </body>
    <script src="https://localhost:8080/js/bundle.js"></script>
</html>
