<?php

namespace App\Http\Resources;

use App\Models\AnimalType;
use App\Models\Pet;
use Illuminate\Http\Resources\Json\JsonResource;

class PetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $animalType = AnimalType::find($this->whenLoaded('breed')->animal_type_id);

        $response = parent::toArray($request);
        $response['animal_type'] = $animalType;

        $response['status'] = [
            'id' => $this->status,
            'label' => $this->status_label
        ];

        $response['gender'] = [
            'id' => $this->gender,
            'label' => $this->gender_label
        ];

        $response['measurement_type'] = [
            'id' => $this->measurement,
            'label' => $this->measurement_label
        ];

        $response['age_type'] = [
            'id' => $this->age_type_id,
            'label' => $this->age_label
        ];

        $response['coat_type'] = [
            'id' => $this->coat_type_id,
            'label' => $this->coat_type_label
        ];

        $weight = $this->weight;
        $height = $this->height;

        $convertedWeight = null;
        $convertedHeight = null;

        if ($this->measurement_type_id == 1){
            $convertedWeight = round($weight / 2.205, 2);
            $convertedHeight = round($height * 2.54, 2);
        }

        if ($this->measurement_type_id == 2){
            $convertedWeight = round($weight * 2.205, 2);
            $convertedHeight = round($height / 2.54, 2);
        }

        $response['measurement']['imperial'] = [
            'weight' => $this->measurement_type_id == 1 ? $weight : $convertedWeight,
            'height' => $this->measurement_type_id == 1 ? $height : $convertedHeight,
        ];

        $response['measurement']['metric'] = [
            'weight' => $this->measurement_type_id == 2 ? $weight : $convertedWeight,
            'height' => $this->measurement_type_id == 2 ? $height : $convertedHeight
        ];

        $response['images'] = ImageResource::collection($this->whenLoaded('images'));


        return $response; 
    }

    public function unitMeasurements()
    {
        $weight = $this->weight;
        $height = $this->height;

        $convertedWeight = null;
        $convertedHeight = null;

        if ($this->measurement_type_id == 1){
            $convertedWeight = round($weight / 2.205, 2);
            $convertedHeight = round($height * 2.54, 2);
        }

        if ($this->measurement_type_id == 2){
            $convertedWeight = round($weight * 2.205, 2);
            $convertedHeight = round($height / 2.54, 2);
        }

        $response['measurement']['imperial'] = [
            'weight' => $this->measurement_type_id == 1 ? $weight : $convertedWeight,
            'height' => $this->measurement_type_id == 1 ? $height : $convertedHeight,
        ];

        $response['measurement']['metric'] = [
            'weight' => $this->measurement_type_id == 2 ? $weight : $convertedWeight,
            'height' => $this->measurement_type_id == 2 ? $height : $convertedHeight
        ];

       
    }

    public function heights(){

        if ($this->measurement_type_id == 1){

        } else if ($this->measurement_type_id == 2){

        }

    }
}
