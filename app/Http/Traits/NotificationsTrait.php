<?php
namespace App\Http\Traits;
use App\Models\Notification;
use Illuminate\Http\Request;

trait NotificationsTrait {

    public function notify(Request $request, Notification $notification){
        $notification = new Notification;
        $notification->user_id = $notification->user_id;
        $notification->object_id = $notification->object_id;
        $notification->object_type = $notification->object_type;
        $notification->message =  $notification->message;
        $notification->save();
        
        // RUN QUEUE
        // PUSH TO PHONE
        // PUSH TO WEB
        
    }

    public function userNotificationsCount($userId, $objectType = null)
    {
        $notifications = null;

        if (!$objectType){
            $notifications = Notification::where('user_id', $userId)
                ->where('has_viewed', false)
                ->get();
        } else {
            $notifications = Notification::where('user_id', $userId)
                ->where('object_type', $objectType)
                ->where('has_viewed', false)
                ->get();
        }

        return (count($notifications));

    }

    public function notificationClear($userId, $objectId, $objectType)
    {
        if ($userId){
            Notification::where('user_id', $userId)
                ->where('object_id', $objectId)
                ->where('object_type', $objectType)
                ->where('has_viewed', false)
                ->update(['has_viewed' => true]);
        }
        

    }
}