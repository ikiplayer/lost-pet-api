<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnimalBreedController extends Controller
{

    /**
     * @urlParam typeId Example: 1
     * @queryParam sortBy Example: name
     * @queryParam descending Example: DESC
     */ 
    
    public function lookup($typeId)
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'name';
        
        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';

        $animalBreed = \App\Models\AnimalBreed::with([])
            ->where('animal_type_id', $typeId)
            ->orderBy($sortBy, $descending)
            ->get();

        return \App\Http\Resources\AnimalBreedResource::collection($animalBreed);
    }
}
