<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessImageMatch;
use App\Models\Image;
use App\Models\ImageTag;
use App\Models\Pet;
use Illuminate\Support\Facades\Storage;
use Spatie\Browsershot\Browsershot;
use Stevebauman\Location\Facades\Location;
use Throwable;

class PetController extends Controller
{

    /**
     * @queryParam sort_by string Sort by [key]. Example: created_at
     * @queryParam status string LOST OR FOUND 1|2. Example: 2
     * @queryParam breed_type object
     * @queryParam breed_type.id integer Example: 1
     * @queryParam descending string DESC|ASC. Example: DESC
     * @queryParam gender string 1|2. Example: 2
     * @queryParam search string Example: golden
     * @queryParam zipcode string Example: 46228
     * @queryParam filter[colors][] string Example: Yellow
     * @queryParam filter[colors][] string Example: Yellow
     * @queryParam filter[ages][0] string Example: Young
     * @queryParam filter[breed] string Example: Golden Retriever
     * @queryParam filter[coat_types][0] integer Example: 1
     * @queryParam filter[animal_types][0] string Example: Dog
     */
    public function index()
    {

        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'lost_date';
        request()->input('status') == 2 ? $status = \App\Models\Pet::STATUS_FOUND : $status = \App\Models\Pet::STATUS_LOST;
        $gender = request()->input('gender');

        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');
        $zipCode = request()->input('zipcode');
        $animalType = request()->input('animal_type');
        $filterColors = is_array(request()->input('filter.colors')) ? request()->input('filter.colors') : null;
        $filterAges = is_array(request()->input('filter.ages')) ? request()->input('filter.ages') : null;
        $filterCoatTypes = is_array(request()->input('filter.coat_types')) ? request()->input('filter.coat_types') : null;
        $filterAnimalTypes = is_array(request()->input('filter.animal_types')) ? request()->input('filter.animal_types') : null;
        $filterBreed =  request()->input('filter.breed');
        $filterLongitude = request()->input('filter.longitude');
        $filterLatitude = request()->input('filter.latitude');
        $filterRadius = request()->input('filter.radius') ? request()->input('filter.radius') : 200;

        $pets = \App\Models\Pet::with(['images', 'breed', 'colors', 'lostLocation', 'foundLocation'])
            ->when($status, function ($query) use ($status){
                return $query->where('status', $status);
            })
            ->when($filterLongitude && $filterLatitude, function ($query) use ($filterLongitude, $filterLatitude, $filterRadius) {
                $subQuery =  $query->whereHas('foundLocation', function ($query) use ($filterLongitude, $filterLatitude, $filterRadius) {
                    return $query->isWithinFoundableMaxDistance($filterLongitude, $filterLatitude, $filterRadius);
                });

                return $subQuery;
            })
            ->when($filterAnimalTypes && count($filterAnimalTypes) > 0, function ($query) use ($filterAnimalTypes){
                return $query->whereHas('breed', function ($query) use ($filterAnimalTypes){
                    return $query->whereIn('animal_type_id', $filterAnimalTypes);
                });
            })
            ->when($filterBreed , function ($query) use ($filterBreed){
                return $query->whereHas('breed', function ($query) use ($filterBreed){
                    return $query->where('name', $filterBreed);
                });
            })
            ->when($filterColors && count($filterColors) > 0, function ($query) use ($filterColors) {
                return $query->whereHas('colors', function ($query) use ($filterColors) {
                    return $query->whereIn('color', $filterColors);
                });
            })
            ->when($filterAges && count($filterAges) > 0, function ($query) use ($filterAges) {
                return $query
                    ->whereIn('age_type_id', $filterAges);
            })
            ->when($filterCoatTypes && count($filterCoatTypes) > 0, function ($query) use ($filterCoatTypes) {
                return $query
                    ->whereIn('coat_type_id', $filterCoatTypes);
            })
            ->when($gender, function ($query) use ($gender){
                return $query->where('gender', $gender);
            })
            ->when($search, function ($query) use ($search){
                return $query
                    ->whereHas('breed', function ($query) use ($search){
                        return $query->where('name',  'LIKE', "%{$search}%");
                    })
                    ->orWhere('name', 'LIKE', "%{$search}%");

            })     
            ->when($zipCode, function ($query) use ($zipCode){
                return 
                    $query->whereHas('lostLocation', function ($query) use ($zipCode) {
                        return $query
                            ->where('zipcode', $zipCode);
                    })
                    ->orWhereHas('foundLocation', function ($query) use ($zipCode) {
                        return $query
                            ->where('zipcode', $zipCode);
                    });
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\PetResource::collection($pets);
    }

    /**
     * @urlParam uuid string
    */

    public function show($uuid)
    {
        $pet = \App\Models\Pet
                ::with(['petComments.user', 'user', 'images', 'breed', 'lostLocation', 'foundLocation'])
                ->where('uuid', $uuid)
                ->first();
                
        if ($pet) {
            return new \App\Http\Resources\PetResource($pet);
        } else {
             return response()->json([
                'message' => __('Cannnot find record.')
               ], 404);
        }
    }

    /**
     * @bodyParam images file[] required
     * @bodyParam animal_type.id integer required Example: 1
     * @bodyParam breed object required
     * @bodyParam breed.id integer required Example: 10
     * @bodyParam age_type object
     * @bodyParam age_type.id integer required Example: 1
     * @bodyParam color_type object
     * @bodyParam color_type.id integer required Example: Brown
     * @bodyParam gender integer required Example: 1
     */

    public function uploadLostPet($request)
    {
        $request->validate([
            'images' => 'required|array|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'animal_type.id' => 'required',
            'breed.id' => 'required',
            'gender' => 'required',
            'color_type.id' => 'required',
            'age_type.id' => 'required'
        ]);

        $uploadedImage = $request->file('image');
        $filename = $uploadedImage->getClientOriginalName();
        $destinationPath = Image::TYPE_LOST_PET;

        $image = new Image;
        $image->src = Storage::putFile($destinationPath, $uploadedImage);
        $image->hashname = basename($image->src);
        $image->name= $filename;
        $image->user_id = $request->user()->id;
        $image->save();

        $imageTag = new ImageTag;
        $imageTag->image_id = $image->id;
        $imageTag->user_animal_type_id = $request->input('animal_type.id');
        $imageTag->user_breed_id = $request->input('breed.id');
        $imageTag->user_age =  $request->input('age_type.id');
        // $imageTag->user_color = $request->input('color_type.id');
        $imageTag->user_weight = $request->input('weight');
        $imageTag->user_height = $request->input('height');
        $imageTag->user_gender = $request->input('gender');
        $imageTag->contact_number = $request->input('contact_number');
        $imageTag->save();

        ProcessImageMatch::dispatch();


        if ($image->exists){
            return new \App\Http\Resources\ImageResource($image);
        } else {
            return response()->json([
            'message' => __('Error uploading image.')
            ], 404);
        }
    }

    public function testProcessImage(){
        $image = Image::with(['lostPetAddress', 'tag'])->whereHas('lostPetAddress', function ($query) {
            return $query->where('lostable_id', '!=', null);
        })->first();

        ProcessImageMatch::dispatch($image);
    }


    public function getHTMLPoster(string $uuid, int $type)
    {
        $pet = \App\Models\Pet
                ::with([ 'images', 'breed.animalType', 'lostLocation', 'user'])
                ->where('uuid', $uuid)
                ->first();
    
        if ($pet) {
            return view('pet.detail', ['pet' => $pet])->render();
        } else {
             return response()->json([
                'message' => __('Cannot find record')
               ], 404);
        }
    }

    // FIXME:
    public function generatePetPoster($uuid, $type)
    {

        $path = Image::TYPE_PET_POSTER.'/'.$uuid.'.png';
        $url = 'http://localhost'.'/pet/'.$uuid.'/'.$type;

        try {
            Browsershot::url($url)
                ->setNodeBinary('/usr/bin/node')
                ->setNpmBinary('/usr/bin/npm')
                ->setChromePath('/usr/bin/google-chrome-unstable')
                ->noSandbox()
                ->timeout(10000)
                ->save($path);
        } catch (Throwable $e) {
            dd($e);
        }
    }


    /**
     * @urlParam uuid string
    */

    public function destroy($id)
    {
        //

        $pet = \App\Models\Pet::find($id);

        if($pet) {
            $pet->delete();
            return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
            return response()->json([
                'message' => __('Record not found')
            ], 404);
        }
    }

    public function getAllPetCountByStatus(){

    }

    

}