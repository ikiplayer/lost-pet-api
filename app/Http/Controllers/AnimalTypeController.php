<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnimalTypeController extends Controller
{

    /**
     * @queryParam sortBy Example: created_at
     * @queryParam descending Example: DESC
     */ 

    public function lookup()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';
        
        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';

        $animalTypes = \App\Models\AnimalType::with([])
            ->orderBy($sortBy, $descending)
            ->get();

        return \App\Http\Resources\AnimalTypeResource::collection($animalTypes);
    }
}
