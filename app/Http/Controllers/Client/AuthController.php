<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\RegistrationValidation;
use Illuminate\Support\Facades\Date;
use Ramsey\Uuid\Uuid;

class AuthController extends Controller
{

    /**
     * @bodyParam first_name string required The first name of user. Example: John
     * @bodyParam last_name string required The last name of user. Example: Doe
     * @bodyParam email string required The email of the user. Example: johndoe@test.com
     * @bodyParam password string required Example: Test123
     * @bodyParam confirm_password string required Example: Test123
     */

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required', 
            'last_name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required|min:6',
            'confirm_password' => 'required',
            'terms' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()->toArray()
            ], 422);
        }

        $user = new \App\Models\User;

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->type = \App\Models\User::TYPE_CLIENT;
        $user->verification_code = Uuid::uuid4()->toString();
        $user->save();
        $user->createOrGetStripeCustomer();


        if ($user){
            $user->notify(new RegistrationValidation($user));
            $token = $user->createToken($user->email.'-'.now());

            return response()->json([
                'data' => [
                    'user' => $user,
                    'token' => $token->accessToken
                ]
            ]);
        } else {
            return response()->json([
                'message' => __('Error saving record.'),
            ], 500);
        }
    }

     /**
     * @bodyParam verification_code string required UUID passed.
     */

    public function verifyRegistration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verification_code' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()->toArray()
            ], 422);
        }

        $user = User::where('verification_code', $request->input('verification_code'))->first();
        $user->verification_code = null;
        $user->email_verified_at = Date::now();
        $user->save();

        if ($user){
            return response()->json(['data' => $user], 200,);
        } else {
            return response()->json([
                'message' => __('Verification error.')
            ], 400);
        }


    }

     /**
     * @bodyParam email string required The email of the user. Example: johndoe@test.com
     * @bodyParam password string required Example: Test123
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required', 
            'password' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json([
                'message' => __('Invalid username and/or password'),
                'data' =>  $validator->errors()->toArray()
            ], 422);
        }

        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password]) ) {
            $user = Auth::user();
            $token = $user->createToken($user->email.'-'.now());

            return response()->json([
                'data' => [
                    'user' => $user,
                    'token' => $token->accessToken
                ]
            ]);
        } else {
            return response()->json([
                'message' => __('Invalid username and/or password'),
                'data' =>  ['message' => ['Invalid username and/or password']]
            ], 422);
        }
    } 

    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'message' => 'Password reset email sent.',
            'data' => $response
        ]);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Email could not be sent to this email address.']);
    }
}
