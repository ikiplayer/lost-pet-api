<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

     /**
     * Creates an intent for payment so we can capture the payment
     * method for the user. 
     * 
     * @param Request $request The request data from the user.
     */
    public function getSetupIntent( Request $request ){
        return $request->user()->createSetupIntent();
    }

    /**
     * Updates a subscription for the user
     * 
     * @param Request $request The request containing subscription update info.
     */

    public function updateSubscription( Request $request ){
        $user = $request->user();
        $planID = $request->input('plan');
        $paymentID = $request->input('payment');

        if( $user->subscribed('default') ){
            $user->newSubscription( 'default', $planID )
                    ->create( $paymentID );
        }else{
            $user->subscription('default')->swap( $planID );
        }
        
        return response()->json([
            'subscription_updated' => true
        ]);
    }
    
    /**
     * Adds a payment method to the current user. 
     * 
     * @param Request $request The request data from the user.
     */

    public function postPaymentMethod( Request $request ){
        $user = $request->user();
        $paymentMethodID = $request->get('payment_method');

        if( $user->stripe_id == null ){
            $user->createAsStripeCustomer();
        }
        // TODO: ONLY HOLD ONE CARD A TIME
        $paymentMethods = $user->paymentMethods();
        foreach( $paymentMethods as $method ){
                $method->delete();
        };

        $user->addPaymentMethod( $paymentMethodID );
        $user->updateDefaultPaymentMethod( $paymentMethodID );
        
        return response()->json( null, 204 );        
    }

     /**
     * Returns the payment methods the user has saved
     * 
     * @param Request $request The request data from the user.
     */
    public function getPaymentMethods( Request $request ){
        $user = $request->user();

        $methods = array();

        if( $user->hasPaymentMethod() ){
            foreach( $user->paymentMethods() as $method ){
                array_push( $methods, [
                    'id' => $method->id,
                    'brand' => $method->card->brand,
                    'last_four' => $method->card->last4,
                    'exp_month' => $method->card->exp_month,
                    'exp_year' => $method->card->exp_year,
                ] );
            }
        }

        return response()->json( $methods );
    }

     /**
     * Removes a payment method for the current user.
     * 
     * @param Request $request The request data from the user.
     */
    public function removePaymentMethod( Request $request ){
        $user = $request->user();
        $paymentMethodID = $request->get('id');

        $paymentMethods = $user->paymentMethods();

        foreach( $paymentMethods as $method ){
            if( $method->id == $paymentMethodID ){
                $method->delete();
                break;
            }
        }

        return response()->json( null, 204 );
    }

}
