<?php

namespace App\Http\Controllers\Client;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PasswordResetController extends Controller
{
        /**
     * @bodyParam email string required Example: johndoe@test.com
     */
    public function createForgot(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()->toArray()
            ], 422);
        }

        $user = \App\Models\User::where('email', $request->input('email'))
            ->where('type', User::TYPE_CLIENT)
            ->first();

            
        if (!$user){
            return response()->json([
                'message' => 'We cant find the user.'
            ], 404);
        }
        
        $passwordReset = \App\Models\PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::uuid()
             ]
        );
        if ($user && $passwordReset)
        {
            $user->notify(
                new \App\Notifications\PasswordForgotRequest($passwordReset->token, $user)
            );
        }
        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    /**
     * @bodyParam email string required Example: johndoe@test.com
     */
    public function createReset(Request $request)
    {

        $user = \App\Models\User::where('id', $request->user()->id)
            ->where('type', User::TYPE_CLIENT)
            ->first();

            
        if (!$user){
            return response()->json([
                'message' => 'We cant find the user.'
            ], 404);
        }
        
        $passwordReset = \App\Models\PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::uuid()
             ]
        );
        if ($user && $passwordReset)
        {
            $user->notify(
                new \App\Notifications\PasswordResetRequest($passwordReset->token, $user)
            );
        }
        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    /**
     * @urlParam token string required
     */

    public function find(Request $request)
    {
        $passwordReset = \App\Models\PasswordReset::where('token', $request->input('token'))
            ->first();

        if (!$passwordReset)
            return response()->json([
                'message' => 'Error'
            ], 404);
        if (\Carbon\Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'Error'
            ], 404);
        }
         return response()->json('Success', 200);
    }

    /**
     * @bodyParam email string required
     * @bodyParam password string required
     * @bodyParam confirm_password string required
     * @bodyParam token string required
     */

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string',
            'confirm_password' => 'required|same:password',
            'token' => 'required|string'
        ]);

        if ($validator->fails()){
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()->toArray()
            ], 422);
        }

        $passwordReset = \App\Models\PasswordReset::where('token', $request->input('token'))->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        $user = \App\Models\User::where('email', $passwordReset->email)->first();
        if (!$user){
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.'
            ], 404);
        }
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        return response()->json('Success', 200);
    }
}
