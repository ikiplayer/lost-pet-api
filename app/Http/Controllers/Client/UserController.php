<?php

namespace App\Http\Controllers\Client;

use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends \App\Http\Controllers\Controller
{
    public function show(Request $request)
    {
        $user = \App\Models\User::with([])->find($request->user()->id);
        if($user) {
            return response()->json(['data' => $user]);
        }

        return response()->json(['message' => 'User not found!'], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $user = \App\Models\User
            ::find($request->user()->id);

        if (!$fails){
            $user = $this->setModel($request, $user);
        } else {
            return response()->json([
                'message' => __('Error updating record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($user->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\UserResource($user)
            ], 200);
        } else {
            return response()->json([
                'message' => __('Error saving record.')
            ], 500);
        }

        
    }


    private function setModel(Request $request, \App\Models\User $user, \App\Models\Address $address = null){

        $user->first_name = ucfirst($request->input('first_name'));
        $user->last_name = ucfirst($request->input('last_name'));
        $user->facebook_id = $request->input('facebook_id');
        $user->venmo_username = $request->input('venmo_username');
        $user->phone_number = $request->input('phone_number');
        $user->about_me_description = $request->input('about_me_description');
        $user->is_registered_pet_public = $request->input('is_registered_pet_public');
        $user->default_measurement_type_id = $request->input('default_measurement_type_id');
        
        $user->save();

        // $address->addressable_id = $user->id;
        // $address->addressable_type = Address::CLASS_TYPE_USER;
        // $address->state = $request->input('address.state');
        // $address->city = $request->input('address.city');
        // $address->address = $request->input('address.address');
        // $address->zipcode = $request->input('address.zipcode');
        // $address->latitude = $request->input('address.latitude');
        // $address->longitude = $request->input('address.longitude');
        // $address->save();
       

        return $user;

    }

    private function getValidator(Request $request, $otherRules = []){

        $rules = [
            'first_name' => 'required',
            'last_name' => 'required'
        ] + $otherRules;

        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }
    
}
