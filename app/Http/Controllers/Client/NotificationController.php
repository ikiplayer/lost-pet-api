<?php

namespace App\Http\Controllers\Client;

use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    /**
     * @queryParam sort_by string Sort by [key]. Example: 'created_at'
     * @queryParam descending string DESC|ASC. Example: DESC
    */
    public function index()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';
        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';

        $notifications = \App\Models\Notification::with(['user'])
            ->where('user_id', request()->user()->id)
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\NotificationResource::collection($notifications);

    }
    
}
