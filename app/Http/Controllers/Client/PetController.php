<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
use Illuminate\Support\Facades\Validator;
use Stevebauman\Location\Facades\Location;
use App\Http\Traits\NotificationsTrait;
use App\Models\Image;
use App\Models\Notification;
use App\Models\Pet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class PetController extends Controller
{
    use NotificationsTrait;

    /**
     * @queryParam sort_by string Sort by [key]. Example: created_at
     * @queryParam status string LOST OR FOUND 1|2. Example: 2
     * @queryParam descending string DESC|ASC. Example: DESC
     * @queryParam search string Example: husky
     * @queryParam gender string 1|2. Example: 2
     * @queryParam search string Example: golden
     * @queryParam zipcode string Example: 46228
     */
    public function index()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';
        request()->input('status') ? $status = request()->input('status') : $status = \App\Models\Pet::STATUS_LOST;
        $gender = request()->input('gender');
        
        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');
        $zipCode = request()->input('zipcode');
        $animalType = request()->input('animal_type');
        $pets = \App\Models\Pet::with(['petComments.user', 'user', 'images', 'breed', 'lostLocation', 'foundLocation'])
            ->where('user_id', request()->user()->id)
            ->when($animalType, function ($query) use ($animalType){
                return $query->whereHas('breed', function ($query) use ($animalType){
                    return $query->where('animal_type_id', $animalType);
                });
            })
            ->when($search, function ($query) use ($search){
                return $query
                    ->whereHas('breed', function ($query) use ($search){
                        return $query->where('name',  'LIKE', "%{$search}%");
                    })
                    ->orWhere('name', 'LIKE', "%{$search}%");
                  
            })
            ->when($gender, function ($query) use ($gender){
                return $query->where('gender', $gender);
            })
            ->when($status, function ($query) use ($status){
                return $query->where('status', $status);
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\PetResource::collection($pets);
    }

    /**
     * @bodyParam name string Example: John Rocky
     * @bodyParam description string Example: I lost it on friday
     * @bodyParam age_type object
     * @bodyParam age_type.id integer Example: 1
     * @bodyParam color_type object
     * @bodyParam color_type_id integer Example: 1
     * @bodyParam weight integer Example: 10
     * @bodyParam height integer Example: 63
     * @bodyParam measurement_type object
     * @bodyParam measurement_type.id integer Example: 1
     * @bodyParam gender integer Example: 1
     * @bodyParam breed_id integer Example: 1
     * @bodyParam is_vaccinated integer Example: 1
     * @bodyParam has_microchip integer Example: 1
     * @bodyParam reward_amount integer Example: 100
     * @bodyParam status object
     * @bodyParam status.id integer Example: 1
     * @bodyParam lost_date string Example: 2021-01-11 03:17:54
     * @bodyParam found_date string Example: 2021-02-15 03:17:54
     * @bodyParam contact_number string Example: 012345678
     * @bodyParam instagram integer Example: test_instgram_user
     * @bodyParam email string Example: test@test.com
     * @bodyParam lost_location object
     * @bodyParam lost_location.country_code string Example: US
     * @bodyParam lost_location.state string Example: Indiana
     * @bodyParam lost_location.city string Example: Fishers
     * @bodyParam lost_location.zipcode string Example: 46228
     * @bodyParam lost_location.address string Example: 123 Main Street
     * @bodyParam lost_location.longitude string Example: 39.9568
     * @bodyParam lost_location.latitude string Example: 86.0134
     * @bodyParam lost_location.lostable_id string
     * @bodyParam found_location object
     * @bodyParam found_location.country_code string Example: US
     * @bodyParam found_location.state string Example: Illinois
     * @bodyParam found_location.city string Example: Chicago
     * @bodyParam lost_location.zipcode string Example: 46228
     * @bodyParam found_location.address string Example: 321 West Street
     * @bodyParam found_location.longitude string Example: 41.8781
     * @bodyParam found_location.latitude string Example: 87.6298
     * @bodyParam found_location.foundable_id string
     */


    public function store(Request $request)
    {
        $validator = $this->getValidator($request, [
            'images' => 'required|array|max:4'
        ]);
        $fails = $validator->fails();

        $pet = new \App\Models\Pet;

        if (!$fails){
            $pet = $this->setModel($request, $pet);
        }  else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }
      

        if ($pet->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\PetResource($pet)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
               ], 500);
        }
    }

    /**
     * @urlParam uuid string
    */
      public function show($uuid)
    {
        //
        $pet = \App\Models\Pet
                ::with(['petComments.user', 'images', 'breed', 'lostLocation', 'foundLocation', 'matchedImages'])
                ->where('uuid', $uuid)
                ->where('user_id', request()->user()->id)
                ->first();

        $this->notificationClear(request()->user()->id, $pet->id, Notification::TYPE_PET_COMMENT);
                
        if ($pet) {
            return new \App\Http\Resources\PetResource($pet);
        } else {
             return response()->json([
                'message' => __('Record not found.')
               ], 404);
        }
    }


    /**
     * @urlParam uuid string
     * @bodyParam name string Example: John Rocky
     * @bodyParam description string Example: I lost it on friday
     * @bodyParam age_type object
     * @bodyParam age_type.id integer Example: 1
    //  * @bodyParam color_type object
    //  * @bodyParam color_type.id integer Example: 1
     * @bodyParam weight integer Example: 10
     * @bodyParam height integer Example: 63
     * @bodyParam measurement_type object
     * @bodyParam measurement_type.id integer Example: 1
     * @bodyParam gender integer Example: 1
     * @bodyParam breed object
     * @bodyParam breed.id integer Example: 1
     * @bodyParam is_vaccinated integer Example: 1
     * @bodyParam has_microchip integer Example: 1
     * @bodyParam microchip_number integer Example: 12345678
     * @bodyParam reward_amount integer Example: 100
     * @bodyParam status object
     * @bodyParam status.id integer Example: 1
     * @bodyParam lost_date string Example: 2021-01-11 03:17:54
     * @bodyParam found_date string Example: 2021-02-15 03:17:54
     * @bodyParam contact_number string Example: 012345678
     * @bodyParam instagram integer Example: test_instgram_user
     * @bodyParam email string Example: test@test.com
     * @bodyParam lost_location object
     * @bodyParam lost_location.country_code string Example: US
     * @bodyParam lost_location.state string Example: Indiana
     * @bodyParam lost_location.city string Example: Fishers
     * @bodyParam lost_location.address string Example: 123 Main Street
     * @bodyParam lost_location.zipcode string Example: 46228
     * @bodyParam lost_location.longitude string Example: 39.9568
     * @bodyParam lost_location.latitude string Example: 86.0134
     * @bodyParam lost_location.lostable_id string
     * @bodyParam found_location object
     * @bodyParam found_location.country_code string Example: US
     * @bodyParam found_location.state string Example: Chicago
     * @bodyParam found_location.city string Example: Illinois
     * @bodyParam found_location.address string Example: 321 West Street
     * @bodyParam found_location.zipcode string Example: 46228
     * @bodyParam found_location.longitude string Example: 41.8781
     * @bodyParam found_location.latitude string Example: 87.6298
     * @bodyParam found_location.foundable_id string
     */
 
    public function update(Request $request, $uuid)
    {
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $pet = \App\Models\Pet
               ::where('uuid', $uuid)
                ->where('user_id', request()->user()->id)
                ->first();

        if (!$fails && $pet){
            $pet = $this->setModel($request, $pet);
        } else {
            return response()->json([
                'message' => __('Error updating record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($pet->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\PetResource($pet)
            ], 200);
        } else {
            return response()->json([
            'message' => __('Error updating record.')
            ], 500);
        }

    }

    public function updatePetFound(Request $request, $uuid)
    {

        $validator = Validator::make($request->all(), [
            'found_date' => 'required|date',
        ]);
          
        $fails = $validator->fails();

        $pet = \App\Models\Pet
                ::with(['petComments.user', 'user', 'images', 'breed', 'lostLocation', 'foundLocation'])
                ->where('uuid', $uuid)
                ->where('user_id', $request->user()->id)
                ->first();


        if (!$fails && $pet){
            $pet->status = Pet::STATUS_FOUND;
            $pet->found_date =  $request->input('found_date');
            $pet->save();
            
        } else {
            return response()->json([
                'message' => __('Error updating record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($pet->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\PetResource($pet)
            ], 200);
        } else {
            return response()->json([
            'message' => __('Error updating record.')
            ], 500);
        }

    }

    /**
     * @urlParam uuid string
     */
    public function destroy($uuid)
    {
        $pet = \App\Models\Pet
            ::where('uuid', $uuid)
            ->where('user_id', request()->user()->id)
            ->first();

        if($pet) {
            $pet->delete();
            return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
            return response()->json([
                'message' => __('Record not found')
            ], 404);
        }
    }

    /**
     * @urlParam uuid string
     * @bodyParam images file[]
     */

    public function addPetImages(Request $request)
    {
        $request->validate([
            'images.*' => 'required|max:2048',
        ]);
        

        $pet = Pet::where('uuid', $request->input('uuid'))->first();
        $uploadedImage = $request->file('image');

        $filename = uniqid().'.'.$uploadedImage->getClientOriginalExtension();    
        $destinationPath = Image::TYPE_LOST_PET;
        $image = new Image;
        $image->name = $uploadedImage->getClientOriginalName();
        $image->src = $uploadedImage->storeAs($destinationPath, $filename, 'public');
        $image->file_name = $filename;
        $image->user_id = $request->user()->id;
        $image->imageable_id = $pet ? $pet->id : null;
        $image->imageable_type = Image::CLASS_TYPE_PET;
        $image->save();



        $foundImage = Image::find($image->id);

        if ($foundImage){
            return new \App\Http\Resources\ImageResource($foundImage);
        } else {
            return response()->json([
                'message' => __('Record(s) not found')
            ], 404);
        }

    }

    /**
     * @urlParam uuid string
     * @urlParam hashname string
     */

    // public function deletePetImage(Request $request, $hashname)
    // {
    //     $image = Image::where('hashname', $hashname)
    //         ->first();

    //     if($image){
    //         $image->delete();

    //         return response()->json([
    //             'message' => __('Record successfully deleted image')
    //         ], 200);

    //     } else {
    //         return response()->json([
    //             'message' => __('Image not found')
    //         ], 200);
    //     }
    // }

    public function petCount()
    {
        $lostPets = Pet::where('user_id', request()->user()->id)
            ->where('status', Pet::STATUS_LOST)
            ->get();

        $foundPets = Pet::where('user_id', request()->user()->id)
            ->where('status', Pet::STATUS_FOUND)
            ->get();

        $registeredPets = Pet::where('user_id', request()->user()->id)
            ->where('status', Pet::STATUS_REGISTERED)
            ->get();

        return response()->json(['data' => [
            'lost_pet_count' => count($lostPets),
            'found_pet_count' => count($foundPets),
            'registered_pet_count' => count($registeredPets)
            ]
        ], 200);

    }

    public function getMyNotifications()
    {
        $userId = request()->user()->id;
        $notifications = Notification::where('user_id', $userId)
            ->orderBy('created_at', 'DESC')
            ->get();

        $notificationCount = $this->userNotificationsCount(request()->user()->id);

        return response()->json([
            'data' => $notifications, 
            'unread_count' => $notificationCount
        ], 200);
    }

    private function setModel(Request $request, \App\Models\Pet $pet){

        $pet->name = $request->input('name');
        $pet->description = $request->input('description');
        $pet->user_id = $request->user()->id;
        $pet->age_type_id = $request->input('age_type.id');
        $pet->coat_type_id = $request->input('coat_type.id');
        $pet->weight = $request->input('weight');
        $pet->height = $request->input('height');
        $pet->measurement_type_id = $request->input('measurement_type_id');
        $pet->gender = $request->input('gender.id');
        $pet->breed_id = $request->input('breed.id');
        $pet->is_vaccinated = $request->input('is_vaccinated');
        $pet->microchip_number = $request->input('microchip_number');
        $pet->reward_amount = $request->input('reward_amount');
        $pet->status = $request->input('status.id');
        $pet->contact_number = $request->input('contact_number');
        $pet->email = $request->input('email');
        $pet->instagram = $request->input('instagram');
        $pet->twitter = $request->input('twitter');


        if ($request->input('status.id') == 1){
            $pet->lost_date = $request->input('lost_date');
        } else if ($request->input('status.id') == 2){
            $pet->found_date = $request->input('found_date');
        }

        $pet->save();

        if ($request->method() == 'POST'){

            $images = $request->input('images');
            if ($images && count($images) == 0) return;

            // if ($request->input('uuid')){
            //       foreach ($images as $key => $uploadedImage) {
            //         $image = Image::find($uploadedImage['id']);
            //         $newImage = new Image;
            //         if ($image){
            //             $newImage->name = $image->name;
            //             $newImage->src = $image->src;
            //             $newImage->url = $image->url;
            //             $newImage->file_name = $image->file_name;
            //             $newImage->imageable_id = $pet->id;
            //             $newImage->imageable_type = $image->imageable_type;
            //             $newImage->user_id = $image->user_id;
            //             $newImage->save();
            //         }
            //     }
            // } else {
            foreach ($images as $key => $uploadedImage) {
                $image = Image::find($uploadedImage['id']);
                if ($image){
                    $image->imageable_id = $pet->id;
                    $image->save();
                }
            }
            // }
        }

        // if ($request->isMethod('POST')){
            
        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_LOST){
        //         $lostAddress = new Address;
        //         $lostAddress->country_code = $request->input('lost_location.country_code');
        //         $lostAddress->state = $request->input('lost_location.state');
        //         $lostAddress->city = $request->input('lost_location.city');
        //         $lostAddress->address = $request->input('lost_location.address');
        //         $lostAddress->zipcode = $request->input('lost_location.zipcode');
        //         $lostAddress->longitude = $request->input('lost_location.longitude');
        //         $lostAddress->latitude = $request->input('lost_location.latitude');
        //         $lostAddress->lostable_id = $pet->id;
        //         $lostAddress->lostable_type = Address::CLASS_TYPE_LOST_PET;
        //         $lostAddress->save();
        //     }

        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_FOUND){
        //         $foundAddress = new Address;
        //         $foundAddress->country_code = $request->input('found_location.country_code');
        //         $foundAddress->state = $request->input('found_location.state');
        //         $foundAddress->city = $request->input('found_location.city');
        //         $foundAddress->address = $request->input('found_location.addrress');
        //         $foundAddress->zipcode = $request->input('found_location.zipcode');
        //         $foundAddress->longitude = $request->input('found_location.longitude');
        //         $foundAddress->latitude = $request->input('found_location.latitude');
        //         $foundAddress->foundable_id = $pet->id;
        //         $foundAddress->foundable_type = Address::CLASS_TYPE_FOUND_PET;
        //         $foundAddress->save();
        //     }
        // } else {

        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_LOST){
        //         $lostAddress = Address::find($request->input('lost_location.id'));
        //         if ($lostAddress){
        //             $lostAddress->country_code = $request->input('lost_location.country_code');
        //             $lostAddress->state = $request->input('lost_location.state');
        //             $lostAddress->city = $request->input('lost_location.city');
        //             $lostAddress->address = $request->input('lost_location.address');
        //             $lostAddress->longitude = $request->input('lost_location.longitude');
        //             $lostAddress->latitude = $request->input('lost_location.latitude');
        //             $lostAddress->lostable_id = $pet->id;
        //             $lostAddress->lostable_type = Address::CLASS_TYPE_LOST_PET;
        //             $lostAddress->save();
        //         }
        //     }

        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_FOUND){
        //         $foundAddress = Address::find($request->input('found_location.id'));
        //         if ($foundAddress){
        //             $foundAddress->country_code = $request->input('found_location.country_code');
        //             $foundAddress->state = $request->input('found_location.state');
        //             $foundAddress->city = $request->input('found_location.city');
        //             $foundAddress->address = $request->input('found_location.addrress');
        //             $foundAddress->longitude = $request->input('found_location.longitude');
        //             $foundAddress->latitude = $request->input('found_location.latitude');
        //             $foundAddress->foundable_id = $pet->id;
        //             $foundAddress->foundable_type = Address::CLASS_TYPE_FOUND_PET;
        //             $foundAddress->save();
        //         }
        //     }
        // }

        $pet->load(['petComments.user', 'user', 'images', 'breed', 'lostLocation', 'foundLocation']);

        return $pet;
    }

    /**
     * @bodyParam images string Example: John Rocky
     * @bodyParam id integer 1
     * @bodyParam type Pet
     * 
    **/
    
    public function uploadImage(Request $request)
    {
        $images = $request->file('images');

        $order = 1;

        $processedImages = [];
        foreach ($images as $key => $image) {
            $savedImage = new Image;
            $filename = uniqid().'.'.$image->getClientOriginalExtension();    
            // $image->src = Storage::putFile('image');
            $savedImage->imageable_id = $request->input('id');
            $savedImage->url = Storage::disk('public')->put($filename, $image);
            $savedImage->name = $image->getClientOriginalName();
            $savedImage->file_name = $filename;
            $savedImage->user_id = $request->user()->id;

            if (strpos($request->url(), '/pet/upload')){
                $savedImage->imageable_type = Image::CLASS_TYPE_PET;
            }
   
            $savedImage->save();
            array_push($processedImages, $savedImage);
        }

         return response()->json([
             'data' => $processedImages
        ], 200);
    }

    /**
     * @urlParam uuid string
     * 
    **/
    

    public function deleteImageByUser($id)
    {
        $image = Image::where('id', $id)
            ->where('user_id', request()->user()->id)
            ->first();
            

        if ($image){
            $image->delete();

            return response()->json([
                'message' => 'Image successfully deleted'
            ], 200);
        } else {
            return response()->json('No image found', 400);
        }
    }

    /**
     * @urlParam uuid string
     * @urlParam userUUID string
     * 
    **/

    public function deleteImageByUUID($uuid, $userUUID)
    {
         $image = Image::where('uuid', $uuid)
            ->where('user_uuid', $userUUID)
            ->first();

        if ($image){
            $image->delete($image->file_name);

            return response()->json([
                'message' => 'Image successfully deleted'
            ], 200);
        } else {
            return response()->json('No image found', 200);
        }

    }

    private function getValidator(Request $request, $otherRules = []){

        $rules = [
            'name' => 'required',
            // 'description' => 'required|min:10',
            'animal_type_id' => 'required',
            'breed.id' => 'required',
            'coat_type.id' => 'required',
            'age_type.id' => 'required',
            'gender' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'lost_date' => ''.($this->checkLostDate($request) ? 'required' : ''),
            'found_date' => ''.($this->checkFoundDate($request) ? 'required' : ''),
        ];

        $messages = [
            'animal_type_id.required' => 'Select an pet type',
            'breed_id.required' => 'Select a breed type',
            'coat_type.id.required' => 'Select a coat type',
            'age_type.id.required' => 'Select an age'            
        ];

        $rules = array_merge($rules, $otherRules);
        $validator = Validator::make($request->all(), $rules, $messages);
        return $validator;
    }

    private function checkLostDate(Request $request)
    {
        if ($request->input('status') == 1){
            return true;
        } else {
            return false;
        }
    }

    private function checkFoundDate(Request $request)
    {
        if ($request->input('status') == 2){
            return true;
        } else {
            return false;
        }
    }


 

}