<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessImageMatch;
use App\Models\Image;
use App\Models\ImageTag;
use App\Models\Pet;
use Illuminate\Support\Facades\Storage;
use Throwable;

class UnidentifiedPetController extends Controller
{

    /**
     * @queryParam sort_by string Sort by [key]. Example: created_at
     * @queryParam status string LOST OR FOUND 1|2. Example: 2
     * @queryParam breed_type object
     * @queryParam breed_type.id integer Example: 1
     * @queryParam descending string DESC|ASC. Example: DESC
     * @queryParam gender string 1|2. Example: 2
     * @queryParam search string Example: golden
     * @queryParam zipcode string Example: 46228
     * @queryParam filter[colors][] string Example: Yellow
     * @queryParam filter[colors][] string Example: Yellow
     * @queryParam filter[ages][0] string Example: Young
     * @queryParam filter[breed] string Example: Golden Retriever
     * @queryParam filter[coat_types][0] integer Example: 1
     * @queryParam filter[animal_types][0] string Example: Dog
     */

    public function index()
    {

        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'lost_date';
        $gender = request()->input('gender');

        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');
        $zipCode = request()->input('zipcode');
        $animalType = request()->input('animal_type');
        $filterColors = is_array(request()->input('filter.colors')) ? request()->input('filter.colors') : null;
        $filterAges = is_array(request()->input('filter.ages')) ? request()->input('filter.ages') : null;
        $filterCoatTypes = is_array(request()->input('filter.coat_types')) ? request()->input('filter.coat_types') : null;
        $filterAnimalTypes = is_array(request()->input('filter.animal_types')) ? request()->input('filter.animal_types') : null;
        $filterBreed =  request()->input('filter.breed');
        $filterLongitude = request()->input('filter.longitude');
        $filterLatitude = request()->input('filter.latitude');
        $filterRadius = request()->input('filter.radius') ? request()->input('filter.radius') : 200;

        $pets = \App\Models\Pet::with(['images', 'breed', 'colors', 'lostLocation', 'foundLocation'])
            ->where('status', Pet::STATUS_UNIDENTIFIED)
            // ->when($filterLongitude && $filterLatitude, function ($query) use ($filterLongitude, $filterLatitude, $filterRadius) {
            //     $subQuery =  $query->whereHas('foundLocation', function ($query) use ($filterLongitude, $filterLatitude, $filterRadius) {
            //         return $query->isWithinFoundableMaxDistance($filterLongitude, $filterLatitude, $filterRadius);
            //     });
            //     return $subQuery;
            // })
            ->when($filterAnimalTypes && count($filterAnimalTypes) > 0, function ($query) use ($filterAnimalTypes){
                return $query->whereHas('breed', function ($query) use ($filterAnimalTypes){
                    return $query->whereIn('animal_type_id', $filterAnimalTypes);
                });
            })
            ->when($filterBreed , function ($query) use ($filterBreed){
                return $query->whereHas('breed', function ($query) use ($filterBreed){
                    return $query->where('name', $filterBreed);
                });
            })
            ->when($filterColors && count($filterColors) > 0, function ($query) use ($filterColors) {
                return $query->whereHas('colors', function ($query) use ($filterColors) {
                    return $query->whereIn('color', $filterColors);
                });
            })
            ->when($filterAges && count($filterAges) > 0, function ($query) use ($filterAges) {
                return $query
                    ->whereIn('age_type_id', $filterAges);
            })
            ->when($filterCoatTypes && count($filterCoatTypes) > 0, function ($query) use ($filterCoatTypes) {
                return $query
                    ->whereIn('coat_type_id', $filterCoatTypes);
            })
            ->when($gender, function ($query) use ($gender){
                return $query->where('gender', $gender);
            })
            ->when($search, function ($query) use ($search){
                return $query
                    ->whereHas('breed', function ($query) use ($search){
                        return $query->where('name',  'LIKE', "%{$search}%");
                    })
                    ->orWhere('name', 'LIKE', "%{$search}%");

            })       
            ->when($zipCode, function ($query) use ($zipCode){
                return 
                    $query->whereHas('lostLocation', function ($query) use ($zipCode) {
                        return $query
                            ->where('zipcode', $zipCode);
                    })
                    ->orWhereHas('foundLocation', function ($query) use ($zipCode) {
                        return $query
                            ->where('zipcode', $zipCode);
                    });
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\PetResource::collection($pets);
    }

    /**
     * @urlParam uuid string
    */

    public function show($uuid)
    {
        $pet = \App\Models\Pet
                ::with(['petComments.user', 'user', 'images', 'breed', 'lostLocation', 'foundLocation'])
                ->where('uuid', $uuid)
                ->first();
                
        if ($pet) {
            return new \App\Http\Resources\PetResource($pet);
        } else {
             return response()->json([
                'message' => __('Cannnot find record.')
               ], 404);
        }
    }


    /**
     * @bodyParam name string Example: John Rocky
     * @bodyParam description string Example: I lost it on friday
     * @bodyParam age_type object
     * @bodyParam age_type.id integer Example: 1
     * @bodyParam color_type object
     * @bodyParam color_type_id integer Example: 1
     * @bodyParam weight integer Example: 10
     * @bodyParam height integer Example: 63
     * @bodyParam measurement_type object
     * @bodyParam measurement_type.id integer Example: 1
     * @bodyParam gender integer Example: 1
     * @bodyParam breed_id integer Example: 1
     * @bodyParam is_vaccinated integer Example: 1
     * @bodyParam has_microchip integer Example: 1
     * @bodyParam reward_amount integer Example: 100
     * @bodyParam status object
     * @bodyParam status.id integer Example: 1
     * @bodyParam lost_date string Example: 2021-01-11 03:17:54
     * @bodyParam found_date string Example: 2021-02-15 03:17:54
     * @bodyParam contact_number string Example: 012345678
     * @bodyParam instagram integer Example: test_instgram_user
     * @bodyParam email string Example: test@test.com
     * @bodyParam lost_location object
     * @bodyParam lost_location.country_code string Example: US
     * @bodyParam lost_location.state string Example: Indiana
     * @bodyParam lost_location.city string Example: Fishers
     * @bodyParam lost_location.zipcode string Example: 46228
     * @bodyParam lost_location.address string Example: 123 Main Street
     * @bodyParam lost_location.longitude string Example: 39.9568
     * @bodyParam lost_location.latitude string Example: 86.0134
     * @bodyParam lost_location.lostable_id string
     * @bodyParam found_location object
     * @bodyParam found_location.country_code string Example: US
     * @bodyParam found_location.state string Example: Illinois
     * @bodyParam found_location.city string Example: Chicago
     * @bodyParam lost_location.zipcode string Example: 46228
     * @bodyParam found_location.address string Example: 321 West Street
     * @bodyParam found_location.longitude string Example: 41.8781
     * @bodyParam found_location.latitude string Example: 87.6298
     * @bodyParam found_location.foundable_id string
     */

    public function store(Request $request)
    {
        //
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $pet = new \App\Models\Pet;

        if (!$fails){
            $pet = $this->setModel($request, $pet);
        }  else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }
      

        if ($pet->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\PetResource($pet)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
               ], 500);
        }
    }


    /**
     * @bodyParam images file[] required
     * @bodyParam animal_type.id integer required Example: 1
     * @bodyParam breed object required
     * @bodyParam breed.id integer required Example: 10
     * @bodyParam age_type object
     * @bodyParam age_type.id integer required Example: 1
     * @bodyParam color_type object
     * @bodyParam color_type.id integer required Example: Brown
     * @bodyParam gender integer required Example: 1
     */

    // public function uploadUnidentifiedPet($request)
    // {
    //     $request->validate([
    //         'images' => 'required|array|mimes:jpeg,png,jpg,gif,svg|max:2048',
    //         'animal_type.id' => 'required',
    //         'breed.id' => 'required',
    //         'gender' => 'required',
    //         'color_type.id' => 'required',
    //         'coat_type.id' => 'required',
    //         'age_type.id' => 'required',
    //         'unidentified_date' => 'required'
    //     ]);

    //     $uploadedImage = $request->file('image');

    //     if ($request->method() == 'POST'){

    //         $images = $request->input('images');
    //         if ($images && count($images) == 0) return;

    //         if ($request->input('uuid')){
    //               foreach ($images as $key => $uploadedImage) {
    //                 $image = Image::find($uploadedImage['id']);
    //                 $newImage = new Image;
    //                 if ($image){
    //                     $newImage->name = $image->name;
    //                     $newImage->src = $image->src;
    //                     $newImage->url = $image->url;
    //                     $newImage->file_name = $image->file_name;
    //                     $newImage->imageable_id = $pet->id;
    //                     $newImage->imageable_type = $image->imageable_type;
    //                     $newImage->user_id = $image->user_id;
    //                     $newImage->save();
    //                 }
    //             }
    //         } else {
    //             foreach ($images as $key => $uploadedImage) {
    //                 $image = Image::find($uploadedImage['id']);
    //                 if ($image){
    //                     $image->imageable_id = $pet->id;
    //                     $image->save();
    //                 }
    //             }
    //         }
    //     }

    //     // ProcessImageMatch::dispatch();

    //     if ($image->exists){
    //         return new \App\Http\Resources\ImageResource($image);
    //     } else {
    //         return response()->json([
    //         'message' => __('Error uploading image.')
    //         ], 404);
    //     }
    // }

    /**
     * @urlParam uuid string
     * @bodyParam images file[]
     */

    public function addPetImages(Request $request)
    {
        $request->validate([
            'images.*' => 'required|max:2048',
        ]);
        
        $pet = Pet::where('uuid', $request->input('uuid'))->first();
        $uploadedImage = $request->file('image');

        $filename = uniqid().'.'.$uploadedImage->getClientOriginalExtension();    
        $destinationPath = Image::TYPE_LOST_PET;
        $image = new Image;
        $image->name = $uploadedImage->getClientOriginalName();
        $image->src = $uploadedImage->storeAs($destinationPath, $filename, 'public');
        $image->file_name = $filename;
        $image->user_id = $request->user()->id;
        $image->imageable_id = $pet ? $pet->id : null;
        $image->imageable_type = Image::CLASS_TYPE_PET;
        $image->save();

        $foundImage = Image::find($image->id);

        if ($foundImage){
            return new \App\Http\Resources\ImageResource($foundImage);
        } else {
            return response()->json([
                'message' => __('Record(s) not found')
            ], 404);
        }
    }

    /**
     * @urlParam uuid string
    */

    public function destroy($id)
    {
        //

        $pet = \App\Models\Pet::find($id);

        if($pet) {
            $pet->delete();
            return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
            return response()->json([
                'message' => __('Record not found')
            ], 404);
        }
    }

    private function setModel(Request $request, \App\Models\Pet $pet){

        $pet->user_id = $request->user()->id;
        $pet->user_uuid = $request->input('user_uuid');
        $pet->age_type_id = $request->input('age_type.id');
        $pet->coat_type_id = $request->input('coat_type.id');
        $pet->weight = $request->input('weight');
        $pet->height = $request->input('height');
        $pet->measurement_type_id = $request->input('measurement_type_id');
        $pet->gender = $request->input('gender.id');
        $pet->breed_id = $request->input('breed.id');
        $pet->status = Pet::STATUS_UNIDENTIFIED;
        $pet->contact_number = $request->input('contact_number');
        $pet->email = $request->input('email');
        $pet->unidentified_date = $request->input('unidentified_date');

        $pet->save();

        if ($request->method() == 'POST'){

            $images = $request->input('images');
            if ($images && count($images) == 0) return;

      
            foreach ($images as $key => $uploadedImage) {
                $image = Image::find($uploadedImage['id']);
                if ($image){
                    $image->imageable_id = $pet->id;
                    $image->save();
                }
            }
        }

        // if ($request->isMethod('POST')){
            
        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_LOST){
        //         $lostAddress = new Address;
        //         $lostAddress->country_code = $request->input('lost_location.country_code');
        //         $lostAddress->state = $request->input('lost_location.state');
        //         $lostAddress->city = $request->input('lost_location.city');
        //         $lostAddress->address = $request->input('lost_location.address');
        //         $lostAddress->zipcode = $request->input('lost_location.zipcode');
        //         $lostAddress->longitude = $request->input('lost_location.longitude');
        //         $lostAddress->latitude = $request->input('lost_location.latitude');
        //         $lostAddress->lostable_id = $pet->id;
        //         $lostAddress->lostable_type = Address::CLASS_TYPE_LOST_PET;
        //         $lostAddress->save();
        //     }

        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_FOUND){
        //         $foundAddress = new Address;
        //         $foundAddress->country_code = $request->input('found_location.country_code');
        //         $foundAddress->state = $request->input('found_location.state');
        //         $foundAddress->city = $request->input('found_location.city');
        //         $foundAddress->address = $request->input('found_location.addrress');
        //         $foundAddress->zipcode = $request->input('found_location.zipcode');
        //         $foundAddress->longitude = $request->input('found_location.longitude');
        //         $foundAddress->latitude = $request->input('found_location.latitude');
        //         $foundAddress->foundable_id = $pet->id;
        //         $foundAddress->foundable_type = Address::CLASS_TYPE_FOUND_PET;
        //         $foundAddress->save();
        //     }
        // } else {

        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_LOST){
        //         $lostAddress = Address::find($request->input('lost_location.id'));
        //         if ($lostAddress){
        //             $lostAddress->country_code = $request->input('lost_location.country_code');
        //             $lostAddress->state = $request->input('lost_location.state');
        //             $lostAddress->city = $request->input('lost_location.city');
        //             $lostAddress->address = $request->input('lost_location.address');
        //             $lostAddress->longitude = $request->input('lost_location.longitude');
        //             $lostAddress->latitude = $request->input('lost_location.latitude');
        //             $lostAddress->lostable_id = $pet->id;
        //             $lostAddress->lostable_type = Address::CLASS_TYPE_LOST_PET;
        //             $lostAddress->save();
        //         }
        //     }

        //     if ($request->input('status.id') == \App\Models\Pet::STATUS_FOUND){
        //         $foundAddress = Address::find($request->input('found_location.id'));
        //         if ($foundAddress){
        //             $foundAddress->country_code = $request->input('found_location.country_code');
        //             $foundAddress->state = $request->input('found_location.state');
        //             $foundAddress->city = $request->input('found_location.city');
        //             $foundAddress->address = $request->input('found_location.addrress');
        //             $foundAddress->longitude = $request->input('found_location.longitude');
        //             $foundAddress->latitude = $request->input('found_location.latitude');
        //             $foundAddress->foundable_id = $pet->id;
        //             $foundAddress->foundable_type = Address::CLASS_TYPE_FOUND_PET;
        //             $foundAddress->save();
        //         }
        //     }
        // }

        $pet->load(['petComments.user', 'user', 'images', 'breed', 'lostLocation', 'foundLocation']);

        return $pet;
    }

    

}