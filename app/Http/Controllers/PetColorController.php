<?php

namespace App\Http\Controllers;

use App\Models\PetColor;
use Illuminate\Http\Request;

class PetColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PetColor  $petColor
     * @return \Illuminate\Http\Response
     */
    public function show(PetColor $petColor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PetColor  $petColor
     * @return \Illuminate\Http\Response
     */
    public function edit(PetColor $petColor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PetColor  $petColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PetColor $petColor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PetColor  $petColor
     * @return \Illuminate\Http\Response
     */
    public function destroy(PetColor $petColor)
    {
        //
    }
}
