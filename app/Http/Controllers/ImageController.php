<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessImageMatch;
use App\Models\Image;
use App\Models\ImageTag;
use Illuminate\Support\Facades\Storage;
use Spatie\Browsershot\Browsershot;
use Stevebauman\Location\Facades\Location;
use Throwable;

class ImageController extends Controller
{

    /**
     * @queryParam sort_by string Sort by [key]. Example: created_at
     * @queryParam status string LOST OR FOUND 1|2. Example: 2
     * @queryParam breed_type object
     * @queryParam breed_type.id integer Example: 1
     * @queryParam descending string DESC|ASC. Example: DESC
     * @queryParam gender string 1|2. Example: 2
     * @queryParam search string Example: golden
     * @queryParam zipcode string Example: 46228
     */
    public function missingPetImages()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'lost_date';
        request()->input('status') == 2 ? $status = \App\Models\Pet::STATUS_FOUND : $status = \App\Models\Pet::STATUS_LOST;
        $gender = request()->input('gender');

        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');
        $zipCode = request()->input('zipcode');
        $animalType = request()->input('animal_type');
        $filterColors = request()->input('filter.colors');
        $filterColors = is_array($filterColors) ? $filterColors : null;
        $filterAges = request()->input('filter.ages');
        $filterAges = is_array($filterAges) ? $filterAges : null;
        $filterCoatTypes = request()->input('filter.coat_types');
        $filterCoatTypes = is_array($filterCoatTypes) ? $filterCoatTypes : null;
        $filterAnimalTypes = request()->input('filter.animal_types');
        $filterAnimalTypes = is_array($filterAnimalTypes) ? $filterAnimalTypes : null;
        $filterBreed =  request()->input('filter.breed');

        // $locationData = Location::get();

        $pets = \App\Models\Pet::with(['images', 'breed', 'colors'])
            ->when($status, function ($query) use ($status){
                return $query->where('status', $status);
            })
            ->when($filterAnimalTypes && count($filterAnimalTypes) > 0, function ($query) use ($filterAnimalTypes){
                return $query->whereHas('breed', function ($query) use ($filterAnimalTypes){
                    return $query->whereIn('animal_type_id', $filterAnimalTypes);
                });
            })
            ->when($filterBreed , function ($query) use ($filterBreed){
                return $query->whereHas('breed', function ($query) use ($filterBreed){
                    return $query->where('name', $filterBreed);
                });
            })
            ->when($filterColors && count($filterColors) > 0, function ($query) use ($filterColors) {
                return $query->whereHas('colors', function ($query) use ($filterColors) {
                    return $query->whereIn('color', $filterColors);
                });
            })
            ->when($filterAges && count($filterAges) > 0, function ($query) use ($filterAges) {
                return $query
                    ->whereIn('age_type_id', $filterAges);
            })
            ->when($filterCoatTypes && count($filterCoatTypes) > 0, function ($query) use ($filterCoatTypes) {
                return $query
                    ->whereIn('coat_type_id', $filterCoatTypes);
            })
            ->when($gender, function ($query) use ($gender){
                return $query->where('gender', $gender);
            })
            ->when($search, function ($query) use ($search){
                return $query
                    ->whereHas('breed', function ($query) use ($search){
                        return $query->where('name',  'LIKE', "%{$search}%");
                    })
                    ->orWhere('name', 'LIKE', "%{$search}%");

            })
            
           
            // ->when($locationData && !$zipCode, function($query) use ($locationData){
            //     return 
            //         $query->whereHas('lostLocation', function ($query) use ($locationData) {
            //             return $query
            //                 ->where('country_code', $locationData->countryCode);
            //         });
               
            // })   
            // ->when($locationData && !$zipCode, function($query) use ($locationData){
            //     return 
            //         $query->whereHas('foundLocation', function ($query) use ($locationData) {
            //             return $query
            //                 ->where('country_code', $locationData->countryCode);
            //         });
            // })         
            ->when($zipCode, function ($query) use ($zipCode){
                return 
                    $query->whereHas('lostLocation', function ($query) use ($zipCode) {
                        return $query
                            ->where('zipcode', $zipCode);
                    })
                    ->orWhereHas('foundLocation', function ($query) use ($zipCode) {
                        return $query
                            ->where('zipcode', $zipCode);
                    });
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\PetResource::collection($pets);
    }

    /**
     * @urlParam uuid string
    */

    public function show($uuid)
    {
        $pet = \App\Models\Pet
                ::with(['petComments.user', 'user', 'images', 'breed', 'lostLocation', 'foundLocation'])
                ->where('uuid', $uuid)
                ->first();
                
        if ($pet) {
            return new \App\Http\Resources\PetResource($pet);
        } else {
             return response()->json([
                'message' => __('Cannnot find record.')
               ], 404);
        }
    } 

}