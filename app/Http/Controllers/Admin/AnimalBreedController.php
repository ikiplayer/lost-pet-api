<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AnimalBreedController extends Controller
{
    /**
     * @queryParam sort_by string Sort by [key]. Example: 'created_at'
     * @queryParam status string LOST OR FOUND 1|2. Example: 2
     * @queryParam search string Example: 'husky'
     * @queryParam descending string DESC|ASC. Example: DESC
     * @queryParam gender string 1|2. Example: 2
     * @queryParam search string Example: golden
     * @queryParam zipcode string Example: 46228
     * @queryParam rows_per_page string
     */
    public function index()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';

        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');

        $animalBreeds = \App\Models\AnimalBreed::with([])
            ->when($search, function ($query) use ($search){
                return $query
                        ->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('breed.name', 'LIKE', "%{$search}%");
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\AnimalBreedResource::collection($animalBreeds);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $animalBreed = new \App\Models\AnimalBreed;

        if (!$fails){
            $animalBreed = $this->setModel($request, $animalBreed);
        }  else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }
      

        if ($animalBreed->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\AnimalBreedResource($animalBreed)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
               ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $animalBreed = \App\Models\AnimalBreed
                ::with([])
                ->find($id);

        if ($animalBreed) {
            return new \App\Http\Resources\AnimalBreedResource($animalBreed);
        } else {
             return response()->json([
                'message' => __('Error saving record.')
               ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $animalBreed = \App\Models\AnimalBreed
                        ::find($id);

        if (!$fails){
            $animalBreed = $this->setModel($request, $animalBreed);
        } else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($animalBreed->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\AnimalBreedResource($animalBreed)
            ], 200);
        } else {
            return response()->json([
            'message' => __('Error saving record.')
            ], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $animalBreed = \App\Models\AnimalBreed::find($id);

        if($animalBreed) {
            $animalBreed->delete();
            return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
            return response()->json([
                'message' => __('Record not found')
            ], 404);
        }
    }

    private function setModel(Request $request, \App\Models\AnimalBreed $animalBreed)
    {
        $animalBreed->name = $request->input('name');
        $animalBreed->description = $request->input('description');
        $animalBreed->animal_type_id = $request->input('animal_type.id');
    }

    private function getValidator(Request $request, $otherRules = [])
    {

        $rules = [
            'product_code' => 'required|min:10',
            'mobile_number' => 'required|min:10',
            'email' => 'required|emali'
        ];

        $rules = array_merge($rules, $otherRules);
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
