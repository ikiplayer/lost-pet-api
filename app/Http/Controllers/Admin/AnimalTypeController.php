<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AnimalTypeController extends Controller
{
    /**
     * @queryParam sort_by string Sort by [key]. Example: 'created_at'
     * @queryParam status string LOST OR FOUND 1|2. Example: 2
     * @queryParam descending string DESC|ASC. Example: DESC
     * @queryParam search string Example: 'dog'
     * @queryParam rows_per_page string
     */

    public function index()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';

        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');

        $animalTypes = \App\Models\AnimalType::with([])
            ->when($search, function ($query) use ($search){
                return $query
                        ->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('breed.name', 'LIKE', "%{$search}%");
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\AnimalTypeResource::collection($animalTypes);
    }

    /**
     * @bodyParam name string Example: Fish
     * @bodyParam description string
     */

    public function store(Request $request)
    {
        //
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $animalType = new \App\Models\AnimalType;

        if (!$fails){
            $animalType = $this->setModel($request, $animalType);
        }  else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }
      

        if ($animalType->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\AnimalTypeResource($animalType)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
               ], 500);
        }
    }

    /**
     * @urlParam id string
     */
    public function show($id)
    {
        //
        $animalType = \App\Models\AnimalType
                ::with([])
                ->find($id);

        if ($animalType) {
            return new \App\Http\Resources\AnimalTypeResource($animalType);
        } else {
             return response()->json([
                'message' => __('Error saving record.')
               ], 404);
        }
    }

    /**
     * @urlParam id string
     * @bodyParam name string Example: Fish
     * @bodyParam description string
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $animalType = \App\Models\AnimalType
                        ::find($id);

        if (!$fails){
            $animalType = $this->setModel($request, $animalType);
        } else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($animalType->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\AnimalTypeResource($animalType)
            ], 200);
        } else {
            return response()->json([
            'message' => __('Error saving record.')
            ], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $animalType = \App\Models\AnimalType::find($id);

        if($animalType) {
            $animalType->delete();
            return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
            return response()->json([
                'message' => __('Record not found')
            ], 404);
        }
    }

    private function setModel(Request $request, \App\Models\AnimalType $animalType)
    {
        $animalType->name = $request->input('name');
        $animalType->description = $request->input('description');
    }

    private function getValidator(Request $request, $otherRules = [])
    {

        $rules = [
            'name' => 'required',
        ];

        $rules = array_merge($rules, $otherRules);
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
