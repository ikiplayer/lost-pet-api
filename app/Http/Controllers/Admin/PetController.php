<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';

        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');

        $pets = \App\Models\Pet::with([])
            ->when($search, function ($query) use ($search){
                return $query
                        ->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('breed.name', 'LIKE', "%{$search}%");
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\PetResource::collection($pets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $pet = new \App\Models\Pet;

        if (!$fails){
            $pet = $this->setModel($request, $pet);
        }  else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }
      

        if ($pet->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\PetResource($pet)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
               ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pet = \App\Models\Pet
                ::with([])
                ->find($id);

        if ($pet) {
            return new \App\Http\Resources\PetResource($pet);
        } else {
             return response()->json([
                'message' => __('Error saving record.')
               ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $pet = \App\Models\Pet
                        ::find($id);

        if (!$fails){
            $pet = $this->setModel($request, $pet);
        } else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($pet->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\PetResource($pet)
            ], 200);
        } else {
            return response()->json([
            'message' => __('Error saving record.')
            ], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $pet = \App\Models\Pet::find($id);

        if($pet) {
            $pet->delete();
            return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
            return response()->json([
                'message' => __('Record not found')
            ], 404);
        }
    }

    private function setModel(Request $request, \App\Models\Pet $pet){

        if ($request->method() == 'post'){
            $pet->activation_date = $request->input('activation_date');
        }

        $pet->name = $request->input('name');
        $pet->description = $request->input('description');
        $pet->user_id = $request->input('owner_id');
        $pet->age = $request->input('age');
        $pet->color = $request->input('color');
        $pet->is_vaccinated = $request->input('is_vaccinated');
        $pet->has_microchip = $request->input('has_microchip');
        $pet->microchip_number = $request->input('microchip_number');
        $pet->reward_amount = $request->input('reward_amount');
        $pet->status = $request->input('status');
        $pet->lost_date = $request->input('lost_date');
        $pet->found_date = $request->input('found_date');
        $pet->email = $request->input('email');
        $pet->status = $request->input('status');

    }

    private function getValidator(Request $request, $otherRules = []){

        $rules = [
            'product_code' => 'required|min:10',
            'mobile_number' => 'required|min:10',
            'email' => 'required|emali'
        ];

        $rules = array_merge($rules, $otherRules);
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}