<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\AnimalBreed;
use App\Models\AnimalType;
use App\Models\Pet;

class LookupController extends Controller
{

    public function petCount()
    {
        $lostPets = \App\Models\Pet
                ::where('status', Pet::STATUS_LOST)
                ->get();

        $foundPets = \App\Models\Pet
                ::where('status', Pet::STATUS_FOUND)
                ->get();

        $registeredPets = \App\Models\Pet
                ::where('status', Pet::STATUS_REGISTERED)
                ->get();
                
        return response()->json([
            'message' => '',
            'data' => [
                'lost_pet_count' => count($lostPets),
                'found_pet_count' => count($foundPets),
                'registered_pet_count' => count($registeredPets)
            ]
        ]);
      
    }


    public function petColors()
    {
        $colors = [
            Pet::COLOR_YELLOW_LABEL,
            Pet::COLOR_BROWN_LABEL,
            Pet::COLOR_WHITE_LABEL,
            Pet::COLOR_BLACK_LABEL,
            Pet::COLOR_GREY_LABEL,
            Pet::COLOR_BLUE_LABEL
        ];

         return response()->json([
            'message' => 'Retrieving all pet colors',
            'data' => $colors
        ]);
    }

    public function petCoatType()
    {
        $coatTypes = [
            ['id' => Pet::COAT_TYPE_HAIRLESS, 'name' => Pet::COAT_TYPE_HAIRLESS_LABEL],
            ['id' => Pet::COAT_TYPE_SHORT, 'name' => Pet::COAT_TYPE_SHORT_LABEL],
            ['id' => Pet::COAT_TYPE_WIRY, 'name' => Pet::COAT_TYPE_WIRY_LABEL],
            ['id' => Pet::COAT_TYPE_MEDIUM, 'name' => Pet::COAT_TYPE_MEDIUM_LABEL],
            ['id' => Pet::COAT_TYPE_LONG, 'name' => Pet::COAT_TYPE_LONG_LABEL],
            ['id' => Pet::COAT_TYPE_CURLY, 'name' => Pet::COAT_TYPE_CURLY_LABEL],
            ['id' => Pet::COAT_TYPE_DOUBLE_COATED, 'name' => Pet::COAT_TYPE_DOUBLE_COATED_LABEL],
        ];

         return response()->json([
            'message' => 'Retrieving all coat types',
            'data' => $coatTypes
        ]);
    }

    public function petBreeds()
    {
        $type = request()->input('type');
        $animalBreeds = AnimalBreed::with([])
            ->when($type, function ($query) use ($type) {
                return $query->where('animal_type_id', $type);
            })
            ->get();
        return \App\Http\Resources\AnimalBreedResource::collection($animalBreeds);
    }

    public function animalTypes()
    {
        $animalTypes = AnimalType::with([])
            ->get();
        return \App\Http\Resources\AnimalTypeResource::collection($animalTypes);

    }


}