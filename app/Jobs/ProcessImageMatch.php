<?php

namespace App\Jobs;

use App\Models\Address;
use App\Models\Image;
use App\Models\ImageTag;
use App\Models\Pet;
use App\Models\PetMatchImage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessImageMatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // MICROSERVICE TO TENSORFLOW NODE.JS
    // TO CHECK ALL THE IMAGES WITHIN A LOCATION RADIUS
    // THEN GET THE TENSORFLOW INFORMATION KEYWORDS
    //
    // THEN TAG THE ALL THE PETS THAT MATCH THAT GENERAL LOCATION

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $image;
    protected $pet;

    public function __construct(Image $image = null, Pet $pet = null)
    {
        //
        $this->image = $image;
        $this->pet = $pet;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // ON LOST PET IMAGE UPLOAD
        if ($this->image){
            $filteredPetsByImageLocation = $this->filterPetsByImageLocation($this->image);
            $this->saveImageMatchesToPets($this->image, $filteredPetsByImageLocation);
        }

        // ON PET LOST CREATED, PET LOST UPDATED LOCATION
        if ($this->pet){
            $filteredMatchImagesByPetLocation = $this->filterMatchImagesByPetLocation($this->pet);
            $this->savePetMatchesToImages($this->pet, $filteredMatchImagesByPetLocation);
        }
    }

    private function filterMatchImagesByPetLocation($pet){

        $petLostLocation = $pet->lostLocation;
        $lostImageAddresses = $this->findNearestImagesByPetLocation($petLostLocation->latitude, $petLostLocation->longitude);
        $lostImageIds = $lostImageAddresses->pluck('addressable_id')->toArray();
        $images = Image::with(['tag'])
            // ->whereHas('tag', function ($query) use ($pet){
            //     return $query
            //         ->where('animal_type_id', $pet->breed->animal_type_id);
            // })
            ->whereIn('id', $lostImageIds)
            ->get();

            dd($images);
        //  return $images;

    }

    private function filterPetsByImageLocation($image){
        $imageTag = $image->tag;
        $imageLostLocation = $image->lostPetAddress;
        $lostImageAddresses = $this->findNearestPetsByImageLocation($imageLostLocation->latitude, $imageLostLocation->longitude);
        $lostPetIds = $lostImageAddresses->pluck('lostable_id')->toArray();
        $pets = Pet::with(['breed', 'age', 'color'])
            ->where('status', Pet::STATUS_LOST)
            // ->whereHas('breed', function ($query) use ($imageTag){
            //     return $query
            //         ->where('animal_type_id', $imageTag->user_animal_type_id);
            //  })
             ->whereIn('id', $lostPetIds)
            ->get();

            dd($pets);

            return $pets;

    }

    // MICROSERVICE TO TENSORFLOW NODE.JS
    private function classifyImageIntegration(){
        

    }

    private function findNearestPetsByImageLocation($latitude, $longitude, $radius = 30)
    {
    
        $lostPetAddresses = Address::
            where('lostable_type', '=', 'App\Models\Pet')
            ->selectRaw("* ,
            ( 3956 * acos( cos( radians(?) )
            * cos( radians( latitude ) )
            * cos( radians( longitude ) - radians(?)) 
            + sin( radians(?) )
            * sin( radians( latitude ) ) )
            ) AS distance", [$latitude, $longitude, $latitude])
            ->having("distance", "<", $radius)
            ->orderBy("distance",'asc')
            ->get();

        return $lostPetAddresses;
    }

    private function findNearestImagesByPetLocation($latitude, $longitude, $radius = 30)
    {
     
        $lostImageAddresses = Address::
            where('addressable_type', 'App\Models\Image')
            ->selectRaw("* ,
            ( 3956 * acos( cos( radians(?) )
            * cos( radians( latitude ) )
            * cos( radians( longitude ) - radians(?)) 
            + sin( radians(?) )
            * sin( radians( latitude ) ) )
            ) AS distance", [$latitude, $longitude, $latitude])
            ->having("distance", "<", $radius)
            ->orderBy("distance",'asc')
            ->get();

        return $lostImageAddresses;
    }

    private function saveImageMatchesToPets(Image $image, $pets = []){
        if (count($pets) == 0) return;
        foreach ($pets as $index => $pet) {
            // TODO: MATCH CALCULATION
            // $this->weightedAverageSimilarity();
            $hasPetMatch = PetMatchImage::where('pet_id', $pet->id)->where('image_id', $image->id)->first();
            if ($hasPetMatch) return;
            $petMatchImage = new PetMatchImage;
            $petMatchImage->pet_id = $pet->id;
            $petMatchImage->image_id = $image->id;
            $petMatchImage->save();
            
        }

    }

    private function savePetMatchesToImages(Pet $pet, $images = []){
        if (count($images) == 0) return;
        foreach ($images as $index => $image) {
            // TODO: MATCH CALCULATION
            // $this->weightedAverageSimilarity();
            $hasPetMatch = PetMatchImage::where('pet_id', $pet->id)->where('image_id', $image->id)->first();
            if ($hasPetMatch) return;
            $petMatchImage = new PetMatchImage;
            $petMatchImage->pet_id = $pet->id;
            $petMatchImage->image_id = $image->id;
            $petMatchImage->save();
            
        }

    }

    public function notifyChannels(){

    }

    public function weightedAverageSimilarity($pet, $image){
    }

    // IF SIMILAR MATCHES ABOVE 30% ADD TO MATCHED IMAGES
    // FIXME SHOULD I FILTER BASED ON 


}
