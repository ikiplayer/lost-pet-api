<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;

class PetMatchImage extends Model
{
    use HasFactory;
    use HasHashid;
    use HashidRouting;

    public function pet()
    {
        return $this->hasOne('App\Model\Pet', 'id', 'pet_id');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    
    
}
