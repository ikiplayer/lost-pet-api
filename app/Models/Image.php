<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Image extends Model
{
    use HasFactory;
    const TYPE_PET = 'pet';
    const TYPE_LOST_PET = 'lost_pet';
    const TYPE_UNIDENTIFIED_PET = 'unidentified_pet';
    const TYPE_PET_POSTER = 'pet_poster';
    
    const CLASS_TYPE_PET = 'App\Models\Pet';
    const CLASS_TYPE_USER = 'App\Models\User';

    public function imageable()
    {
        return $this->morphTo();
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function lostPetAddress()
    {
        return $this->morphOne(Address::class, 'lostable');
    }

    public function foundPetAddress()
    {
        return $this->morphOne(Address::class, 'foundable');
    }


    public function matchedPets()
    {
        return $this->belongsToMany('App\Models\Pet', 'pet_match_images', 'image_id', 'pet_id');
    }

    public function tag()
    {
        return $this->hasOne('App\Models\ImageTag', 'image_id', 'id');
    }

}
