<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class ImageTag extends Model
{
    use HasFactory;

    public function petMatchImage()
    {
        return $this->belongsTo('App\Models\Image', 'id', 'image_id');
    }

    public function colors()
    {
        return $this->morphMany(PetColor::class, 'colorable');
    }
}
