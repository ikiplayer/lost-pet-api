<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;

class AnimalBreed extends Model
{
    use HasFactory;
    use HasHashid;
    use HashidRouting;

    public function animalType()
    {
        
        return $this->hasOne('App\Models\AnimalType', 'id', 'animal_type_id');
    }
}
