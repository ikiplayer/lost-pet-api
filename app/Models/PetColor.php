<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;

class PetColor extends Model
{
    use HasFactory;
    use HasHashid;
    use HashidRouting;

    const COLOR_YELLOW_LABEL = 'Yellow';
    const COLOR_BROWN_LABEL = 'Brown';
    const COLOR_WHITE_LABEL = 'White';
    const COLOR_BLACK_LABEL = 'Black';
    const COLOR_GREY_LABEL = 'Grey';
    const COLOR_BLUE_LABEL = 'Blue';

    const CLASS_TYPE_PET = 'App\Models\Pet';
    const CLASS_TYPE_IMAGE_TAG = 'App\Models\ImageTag';

    const TYPE_AI = 1;
    const TYPE_USER = 1;

    public function colorable()
    {
        return $this->morphTo();
    }
}
