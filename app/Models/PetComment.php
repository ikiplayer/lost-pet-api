<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;

class PetComment extends Model
{
    use HasFactory;
    use HasHashid;
    use HashidRouting;

    public function pet()
    {
        return $this->belongsTo('App\Models\Pet', 'id', 'pet_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
