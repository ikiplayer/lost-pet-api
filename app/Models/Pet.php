<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Webpatser\Uuid\Uuid;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;


class Pet extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasHashid;
    use HashidRouting; 

    const STATUS_LOST = 1;
    const STATUS_FOUND = 2;
    const STATUS_REGISTERED = 3;
    const STATUS_PENDING = 4;
    const STATUS_REJECTED = 5;
    const STATUS_UNIDENTIFIED = 6;
    const STATUS_PENDING_LABEL = 'Pending';
    const STATUS_LOST_LABEL = 'Lost';
    const STATUS_FOUND_LABEL = 'Found';
    const STATUS_REGISTERED_LABEL = 'Registered';
    const STATUS_REJECTED_LABEL = 'Rejected';
    const STATUS_UNIDENTIFIED_LABEL = 'Unidentified';
    

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_MALE_LABEL = 'Male';
    const GENDER_FEMALE_LABEL = 'Female';

    const SYSTEM_IMPERIAL = 0;
    const SYSTEM_METRIC = 1;
    const SYSTEM_IMPERIAL_LABEL = 'Imperial';
    const SYSTEM_METRIC_LABEL = 'Metric';

    const AGE_YOUNG = 1;
    const AGE_ADOLESCENCE = 2;
    const AGE_ADULT = 3;
    const AGE_SENIOR = 4;
    const AGE_YOUNG_LABEL = 'Young';
    const AGE_ADOLESCENCE_LABEL = 'Adolescence';
    const AGE_ADULT_LABEL = 'Adult';
    const AGE_SENIOR_LABEL = 'Senior';

    const COLOR_YELLOW_LABEL = 'Yellow';
    const COLOR_BROWN_LABEL = 'Brown';
    const COLOR_WHITE_LABEL = 'White';
    const COLOR_BLACK_LABEL = 'Black';
    const COLOR_GREY_LABEL = 'Grey';
    const COLOR_BLUE_LABEL = 'Blue';

    const COAT_TYPE_HAIRLESS = 1;
    const COAT_TYPE_SHORT = 2;
    const COAT_TYPE_WIRY = 3;
    const COAT_TYPE_LONG = 4;
    const COAT_TYPE_CURLY = 5;
    const COAT_TYPE_DOUBLE_COATED = 6;
    const COAT_TYPE_MEDIUM = 7;
    const COAT_TYPE_HAIRLESS_LABEL = 'Hairless';
    const COAT_TYPE_SHORT_LABEL = 'Short';
    const COAT_TYPE_WIRY_LABEL = 'Wiry';
    const COAT_TYPE_MEDIUM_LABEL = 'Medium';
    const COAT_TYPE_LONG_LABEL = 'Long';
    const COAT_TYPE_CURLY_LABEL = 'Curly';
    const COAT_TYPE_DOUBLE_COATED_LABEL = 'Double Coated';

    public function messages()
    {
        return [
            'animal_type_id.required' => 'Select an pet type'
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function breed()
    {
        return $this->hasOne('App\Models\AnimalBreed', 'id', 'breed_id');
    }

    public function color()
    {
        return $this->hasOne('App\Models\AnimalBreed', 'id', 'breed_id');
    }

    public function age()
    {
        return $this->hasOne('App\Models\AnimalBreed', 'id', 'breed_id');
    }

    public function colors()
    {
        return $this->morphMany(PetColor::class, 'colorable');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function lostLocation()
    {
        return $this->morphOne(Address::class, 'lostable');
    }

    public function foundLocation()
    {
        return $this->morphOne(Address::class, 'foundable');
    }

    public function petComments()
    {
        return $this->hasMany('App\Models\PetComment', 'pet_id', 'id');
    }

    public function matchedImages()
    {
        return $this->belongsToMany('App\Models\Image', 'pet_match_images', 'pet_id', 'image_id');
    }

    public function getStatusLabelAttribute()
    {
        $label = '';
        switch ($this->status) {
            case self::STATUS_PENDING:
                $label = __(self::STATUS_PENDING_LABEL);
                break;
            case self::STATUS_LOST:
                $label = __(self::STATUS_LOST_LABEL);
                break;
            case self::STATUS_FOUND:
                $label = __(self::STATUS_FOUND_LABEL);
                break;
            case self::STATUS_REGISTERED:
                $label = __(self::STATUS_REGISTERED_LABEL);
                break;
            case self::STATUS_REJECTED:
                $label = __(self::STATUS_REJECTED_LABEL);
                break;
            case self::STATUS_UNIDENTIFIED:
                $label = __(self::STATUS_UNIDENTIFIED_LABEL);
                break;
        }

        return $label;
    }

    public function getGenderLabelAttribute()
    {
        $label = '';
        switch ($this->gender) {
            case self::GENDER_MALE:
                $label = __(self::GENDER_MALE_LABEL);
                break;
            case self::GENDER_FEMALE:
                $label = __(self::GENDER_FEMALE_LABEL);
                break;
        }

        return $label;
    }

    public function getMeasurementLabelAttribute()
    {
        $label = '';
        switch ($this->measurement_type_id) {
            case self::SYSTEM_IMPERIAL:
                $label = __(self::SYSTEM_IMPERIAL_LABEL);
                break;
            case self::SYSTEM_METRIC:
                $label = __(self::SYSTEM_METRIC_LABEL);
                break;
        }

        return $label;
    }

    public function getAgeLabelAttribute()
    {
        $label = '';
        switch ($this->age_type_id) {
            case self::AGE_YOUNG:
                $label = __(self::AGE_YOUNG_LABEL);
                break;
            case self::AGE_ADOLESCENCE:
                $label = __(self::AGE_ADOLESCENCE_LABEL);
                break;
            case self::AGE_ADULT:
                $label = __(self::AGE_ADULT_LABEL);
                break;
            case self::AGE_SENIOR:
                $label = __(self::AGE_SENIOR_LABEL);
                break;
        }

        return $label;
    }

    public function getCoatTypeLabelAttribute()
    {
        $label = '';
        switch ($this->coat_type_id) {
            case self::COAT_TYPE_HAIRLESS:
                $label = __(self::COAT_TYPE_HAIRLESS_LABEL);
                break;
            case self::COAT_TYPE_SHORT:
                $label = __(self::COAT_TYPE_SHORT_LABEL);
                break;
            case self::COAT_TYPE_WIRY:
                $label = __(self::COAT_TYPE_WIRY_LABEL);
                break;
          case self::COAT_TYPE_MEDIUM:
                $label = __(self::COAT_TYPE_MEDIUM_LABEL);
                break;
            case self::COAT_TYPE_LONG:
                $label = __(self::COAT_TYPE_LONG_LABEL);
                break;
            case self::COAT_TYPE_CURLY:
                $label = __(self::COAT_TYPE_CURLY_LABEL);
                break;
            case self::COAT_TYPE_DOUBLE_COATED:
                $label = __(self::COAT_TYPE_DOUBLE_COATED_LABEL);
                break;
        }

        return $label;
    }




    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) uniqid();
        });
    }

    
}
