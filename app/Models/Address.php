<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;

class Address extends Model
{
    use HasFactory;
    use HasHashid;
    use HashidRouting;


    const CLASS_TYPE_USER = 'App\Models\User';
    const CLASS_TYPE_LOST_PET = 'App\Models\Pet';
    const CLASS_TYPE_FOUND_PET = 'App\Models\Pet';
    const CLASS_TYPE_IMAGE_PET = 'App\Models\Image';


    public function addressable()
    {
        return $this->morphTo();
    }

    public function lostable()
    {
        return $this->morphTo();
    }

    public function foundable()
    {
        return $this->morphTo();
    }

    public function unidentifiable(){
        return $this->morphTo();
    }

    public function scopeIsWithinLostableMaxDistance($query, $latitude, $longitude, $radius = 200)
    {

    $haversine = "(6371 * acos(cos(radians(" . $latitude . ")) 
                    * cos(radians(`latitude`)) 
                    * cos(radians(`longitude`) 
                    - radians(" . $longitude . ")) 
                    + sin(radians(" . $latitude . ")) 
                    * sin(radians(`latitude`))))";


    return $query->select('lostable_id')
                 ->selectRaw("{$haversine} AS distance")
                 ->whereRaw("{$haversine} < ?", [$radius]);
    }

    public function scopeIsWithinFoundableMaxDistance($query, $latitude, $longitude, $radius = 200)
    {

    $haversine = "(6371 * acos(cos(radians(" . $latitude . ")) 
                    * cos(radians(`latitude`)) 
                    * cos(radians(`longitude`) 
                    - radians(" . $longitude . ")) 
                    + sin(radians(" . $latitude . ")) 
                    * sin(radians(`latitude`))))";


    return $query->select('foundable_id')
                 ->selectRaw("{$haversine} AS distance")
                 ->whereRaw("{$haversine} < ?", [$radius]);
    }
}
