<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    const TYPE_MATCHED_PET = 0;
    const TYPE_PET_COMMENT = 1;
    const TYPE_FOUND_PET = 2;
    const TYPE_SUBSCRIPTION = 3;

   
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id', 'user_id');
    }

}
